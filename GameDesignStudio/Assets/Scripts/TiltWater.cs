﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TiltWater : MonoBehaviour {

	public Transform spriteTransform;
	public Vector3 posOffset;
	public Vector3 startPos;

	private GenerateFruits generateFruits;
	private BuoyancyEffector2D be2d;

	public float waterSpeed;
	private Vector3 lastAccel;
	private Vector3 accel;

	public float jumpPower;
	public float jumpTolerance;
	public float JumpResetTime;
	private bool jumping;
	private float jumpDifference;

	// Use this for initialization
	void Start () {
		generateFruits = GetComponent<GenerateFruits>();
		be2d = GetComponent<BuoyancyEffector2D>();
	}
	
	// Update is called once per frame
	void Update () {
		lastAccel = accel;
		accel = Input.acceleration;

#if UNITY_EDITOR
		if(Input.GetKeyDown(KeyCode.LeftArrow))
		{
			accel.x = -1f;
		}
		else if (Input.GetKeyDown(KeyCode.RightArrow))
		{
			accel.x = 1f;
		}

		if(Input.GetKeyDown(KeyCode.Space))
		{
			accel.y = 0.3f;
		}
#endif

		if (accel.x != 0f)
		{
			float speed = (-1f / (accel.x * accel.x)) + waterSpeed;
			if(speed > 0)
			{
				be2d.flowMagnitude = speed * ((accel.x > 0f) ? 1f : -1f);
				be2d.flowVariation = Mathf.Abs(speed * 0.5f);

			}
		}

		if(generateFruits.currentCount > 0)
		{
			if (!jumping)
			{
				Jump();
				// Visual feedback for the jumping the fruits
				spriteTransform.position = Vector3.Lerp(spriteTransform.position,
					startPos, Time.deltaTime * 10f);
			}
			else
			{
				// Return the water level back to normal
				spriteTransform.position = Vector3.Lerp(spriteTransform.position,
					startPos + posOffset, Time.deltaTime);
			}
		}
		else
		{
			// Hide the water level when there are no fruits
			spriteTransform.position = Vector3.Lerp(spriteTransform.position,
					startPos + new Vector3(0f, -5f, 0f), Time.deltaTime * 10f);
		}

	}

	void Jump()
	{
		jumpDifference = accel.y - lastAccel.y;
		float jumpScale = jumpDifference - jumpTolerance;
		if (jumpScale > 0)
		{
			be2d.density = jumpPower * (1f + jumpScale);
			jumping = true;
			StartCoroutine(ResetJump());
		}
	}

	IEnumerator ResetJump()
	{
		yield return new WaitForSeconds(JumpResetTime);
		be2d.density = 1.9f;
		jumpDifference = 0f;
		jumping = false;
	}
}
