﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class EndGameReferences : MonoBehaviour {

	public GameManager gm;
	public Text winnerText;

	void Start()
	{
		gm = FindObjectOfType<GameManager>();
		UpdateWinner();
	}

	void UpdateWinner()
	{
		List<string> winnerList = gm.GetWinners();
		winnerText.text = "Congrats to the winners:";
		foreach(string winner in winnerList)
		{
			winnerText.text += " " + winner + ",";
		}
		winnerText.text = winnerText.text.Remove(winnerText.text.Length - 1);
	}

	public void DestroyGM()
	{
		if(gm != null) Destroy(gm.gameObject);
	}
}
