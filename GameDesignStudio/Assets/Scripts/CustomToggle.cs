﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomToggle : MonoBehaviour {

	//private Toggle toggle;

	public RectTransform onOffTransform;
	public RectTransform knobTransform;
	public AnimationCurve linearCurve;
	public float toggleTime;

	private Vector3 knobOffset = new Vector3(-163f, 0f, 0f)/* / 1440f*/;
	private Vector3 sliderOffset = new Vector3(97.5f, 0f, 0f)/* / 1440f*/;
	private bool canToggle;

	public bool startRandomized;

	private Toggle toggle;

	// Use this for initialization
	void Start () {

		toggle = GetComponent<Toggle>();
		canToggle = true;
		//knobOffset *= Screen.height;
		//sliderOffset *= Screen.height;

		// Do it at the start to fix the scaling issues making it look bad
		// Randomised so you don't know what is a lie
		if(startRandomized)
		{
			bool startValue = (Random.value >= 0.5f) ? true : false;
			toggle.isOn = startValue;
		}
	}
	
	public void MoveKnob(Vector3 x)
	{
		knobTransform.anchoredPosition3D = x;
	}

	public void MoveSlider(Vector3 x)
	{
		onOffTransform.anchoredPosition3D = x;
	}

	public void ChangeValue(bool val)
	{
		if(canToggle)
		{
			canToggle = false;
			CoroutineAnims.CoroutineHandler(CoroutineAnims.LerpVector3(
				0f, toggleTime, linearCurve, 
				(val) ? knobOffset : Vector3.zero, 
				(val) ? Vector3.zero : knobOffset,
				MoveKnob), CanToggle);
			CoroutineAnims.CoroutineHandler(CoroutineAnims.LerpVector3(
				0f, toggleTime, linearCurve,
				(val) ? -sliderOffset : sliderOffset,
				(val) ? sliderOffset : -sliderOffset,
				MoveSlider));
		}
	}

	private void CanToggle(bool unused)
	{
		canToggle = true;
	}

	public void ToggleToggle()
	{
		toggle.isOn = !toggle.isOn;
	}
}
