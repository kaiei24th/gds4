﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
	public bool shufflePlayers;

	public SceneCtrl.Scenes lastGameState;
	public SceneCtrl.Scenes gameState;

	public int currentRound;
	public int maxRounds;

	//[Range(0, 1)] public float ratio;

	public int currentGM;

	// This a list index not the id of the PlayerData
	// So work around out of bounds and index mismatches
	public int currentPlayer;

	//public int numSabotagers;

	// This is also a list index
	public int currentDeck;

	// The current prompt
	//public int currentPrompt;

	// Current Time
	public float turnTime;
	// Max Time
	public float turnTimer;

	// We make a copy so we don't lose our prompts cos we delete them so that
	// players don't choose the same prompt
	/*[HideInInspector]*/ public List<string> availableHints;
	/*[HideInInspector]*/ public List<string> choosenHints;
	public List<string> hints;
	public List<PlayerData> players;
	//public List<PromptBase> prompts;
	//public List<ConditionBase> conditions;
	//public List<SkillBase> skillsAvailable;
	public List<Interactions> decks;

	// A Reference to the current player turn references 
	// so we can possibly pass them into the skill 
	[HideInInspector] public PlayerTurnReferences ptr;

	// Normal
	public const int playerPointMatch = 10;
	public const int playerPointMatchPerfect = 20;
	// Bonus
	public const int playerPoints2PerfectMatches = 20;
	public const int playerPoints3PerfectMatches = 40;

	public const int fmPointsLostMatch = -10;
	public const int fmPointsMislead = 20;

	void Start()
	{
		availableHints = hints.ToList();
	}

	void Update()
	{
		//turnTime += Time.deltaTime;
	}

	public PlayerData GetCurrentPlayer()
	{
		return players[currentPlayer];
	}

	public Interactions GetCurrentDeck()
	{
		return decks[currentDeck];
	}

// 	public PromptBase GetCurrentPrompt()
// 	{
// 		return prompts[currentPrompt];
// 	}

	public string GetCardName(int idx)
	{
		return GetCurrentDeck().cards[idx];
	}

// 	// Enable a skill
// 	public void EnableSkills(int selectedSkill, int target)
// 	{
// 		GetCurrentPlayer().skillStorage[selectedSkill].
// 			EnableSkill(this, currentPlayer, target);
// 		GetCurrentPlayer().skillStorage.RemoveAt(selectedSkill);
// 	}
// 
// 	// General Skill activation
// 	public void ActivateSkills(int instigator, int target, 
// 		SkillBase.ApplicationType appType)
// 	{
// 		PlayerData pd = players[instigator];
// 		if (pd.skillsApplied.Count == 0) return;
// 		// Activate each skill
// 		foreach (SkillBase sb in pd.skillsApplied)
// 		{
// 			if(sb.appType == appType)
// 			{
// 				// We use the skill's instigator
// 				sb.ActivateSkill(this, sb.instigator, target);
// 				sb.applicationTime--;
// 			}
// 		}
// 		// Remove all the expired skills
// 		pd.skillsApplied.RemoveAll(x => x.applicationTime == 0);
// 	}

	// Called on from the start game button in lobby
	public void StartGame()
	{
		currentGM = 0;
		currentPlayer = 0;
		currentRound = 0;
		turnTime = 0;
		//EndRound();
	}

// 	void InitPrompts()
// 	{
// 		foreach(PromptBase pb in prompts)
// 		{
// 			pb.Create(this);
// 		}
// 	}

// 	public void RandomizeSides()
// 	{
// 		// 3 players - 1 sabotagers
// 		// 4 players - 2 sabotagers
// 		// 5 players - 2 sabotagers
// 		// 6 players - 2 sabotagers
// 		// 7 players - 3 sabotagers
// 		// 8 players - 3 sabotagers
// 		if (ratio == 0) ratio = 1;
// 		int evilRatio = Mathf.RoundToInt((float)players.Count * ratio);
// 		System.Random rnd = new System.Random();
// 		Debug.Log(evilRatio + " sabotagers");
// 		List<PlayerData> newEvil = players.
// 			OrderBy(x => rnd.Next()).Take(evilRatio).ToList();
// 		foreach(PlayerData pd in players)
// 		{
// 			if(newEvil.Contains(pd))
// 			{
// 				pd.side = PlayerData.Side.Sabotagers;
// 			}
// 			else
// 			{
// 				pd.side = PlayerData.Side.Mixers;
// 			}
// 		}
// 	}

	// Get a list of other sabotagers
// 	public List<string> GetSabotagers()
// 	{
// 		List<string> bad = new List<string>();
// 		foreach(PlayerData pd in players)
// 		{
// 			if(pd.side == PlayerData.Side.Sabotagers && 
// 				pd.id != GetCurrentPlayer().id)
// 			{
// 				bad.Add(pd.name);
// 			}
// 		}
// 		return bad;
// 	}

	// Get all the winners
	public List<string> GetWinners()
	{
		List<string> winnerList = new List<string>();

		float highestScore = 0;

		foreach(PlayerData pd in players)
		{
			if(pd.score > highestScore)
			{
				highestScore = pd.score;
				winnerList = new List<string>();
				winnerList.Add(pd.name);
			}
			else if (pd.score == highestScore)
			{
				winnerList.Add(pd.name);
			}
		}
		return winnerList;
	}

// 	public void EndRound()
// 	{
// 		RandomizeSides();
// 		// Reset prompts
// 		InitPrompts();
// 		// New Prompt
// 		currentPrompt = Random.Range(0, prompts.Count);
// 	}

	// Called from PTR UpdateSelection(int selection)
// 	public void UpdateSelection(int selection)
// 	{
// 		players[currentPlayer].choosenItem = selection;
// 	}

	// Called when we need to change the scene
	public void LoadScene(SceneCtrl.Scenes scene)
	{
		lastGameState = gameState;
		gameState = scene;
		SceneCtrl.Load(scene);
	}

	// Called from PTR EndTurn()
	public void EndTurn()
	{
		if (SetNextPlayer(true)) LoadScene(SceneCtrl.Scenes.EndRound);
		else LoadScene(SceneCtrl.Scenes.InfoReveal);
	}

	public bool SetNextPlayer(bool changeRound)
	{
		currentPlayer++;
		if (currentPlayer == players.Count)
		{
			if(changeRound) currentRound++;
			currentPlayer = 0;
			return true;
		}
		return false;
	}

	// Called by EndRoundReferences
// 	public void CalculateEndTurn()
// 	{
// 		float comp = GetCurrentPrompt().complete;
// 		PlayerData.Side winners;
// 		if (comp >= GetCurrentPrompt().minimum)
// 			winners = PlayerData.Side.Mixers;
// 		else winners = PlayerData.Side.Sabotagers;
// 
// 		for (int i = 0; i < players.Count; ++i)
// 		{
// 			PlayerData pd = players[i];
// 
// 			pd.earned = (pd.side == winners) ? 1 : 0;
// 			pd.score += pd.earned;
// //			int roundEarned = 0;
// // 			int fruit = pd.choosenItem;
// // 
// // 			// They didn't choose or couldn't choose a fruit
// // 			if (fruit == -1)
// // 			{
// // 				// Negative score mode
// // 				//finalScore = -(players.Count - 1);
// // 				roundEarned = 0;
// // 			}
// // 			else
// // 			{
// // 				// Calculate score
// // 
// // // 				foreach (PlayerData other in players)
// // // 				{
// // // 					if (pd.id == other.id) continue;
// // // 					finalScore += GetCurrentDeck().
// // // 						WinsAgainst(fruit, other.choosenItem) ? 1 : 0;
// // // 					//if (fruit == other.choosenItem) finalScore += 1;
// // // 				}
// // 
// // // 				// Apply End Round Skills
// // // 				ActivateSkills(i, i, SkillBase.ApplicationType.EndRound);
// // 			}
// 
// 			//pd.earned = roundEarned;
// 			//pd.score += roundEarned;
// 		}
// 
// // 		// Check Conditions
// // 		for (int i = 0; i < players.Count; ++i)
// // 		{
// // 			PlayerData pd = players[i];
// // 			for (int j = 0; j < conditions.Count; ++j)
// // 			{
// // 				ConditionBase cb = conditions[j];
// // 				if(cb.available && cb.MetCondition(this, i))
// // 				{
// // 					SkillBase newSB = Object.Instantiate(skillsAvailable[j]) as SkillBase;
// // 					newSB.instigator = i;
// // 					if (newSB == null) Debug.Log("Fail");
// // 					else Debug.Log("add " + newSB.name);
// // 					pd.skillStorage.Add(newSB);
// // 					pd.AddLog("You have gained the " + 
// // 						pd.skillStorage.Last().name + " skill");
// // 					cb.available = false;
// // 				}
// // 			}
// // 		}
// // 
// // 		for(int i = conditions.Count - 1; i >= 0; --i)
// // 		{
// // 			if(!conditions[i].available)
// // 			{
// // 				// Set it back to true for the next time
// // 				Debug.Log("remove " + i);
// // 				conditions[i].available = true;
// // 				skillsAvailable.RemoveAt(i);
// // 				conditions.RemoveAt(i);
// // 			}
// // 		}
// 
// 		foreach (PlayerData pd in players)
// 		{
// 			pd.choosenItem = -1;
// 		}
// 	}

	// This is the 3th ideas calculate end round
	public void NewCalculateEndTurn()
	{
		List<int> listToMatch = players[currentGM].fruitList.ToList();

		int GMScore = players.Count - 1;

		for(int i = 0; i < players.Count; ++i)
		{
			if (i == currentGM) continue;
			PlayerData pd = players[i];
			List<int> list = pd.fruitList.ToList();
			if(CheckListsPerfect(list, listToMatch))
			{
				pd.earned = 2;
				pd.score += pd.earned;
				GMScore--;
			}
			else if(CheckLists(list, listToMatch))
			{
				pd.earned = 1;
				pd.score += pd.earned;
				GMScore--;
			}
		}

		players[currentGM].earned = GMScore;
		players[currentGM].score += players[currentGM].earned;

		// Reset / Change vars here cos Im lazy
		//currentGM = (currentGM + 1) % players.Count;
		//currentPlayer = currentGM;
		choosenHints.Clear();
		availableHints = hints.ToList();
	}

	public int GetBonusPoints(int playerIdx)
	{
		List<int> list;
		List<int> listToMatch;
		PlayerData pd;

		// Bonus points for FM who misleads players
		int numMisled = 0;
		for (int i = 0; i < players.Count; ++i)
		{
			if (i == currentGM) continue;
			pd = players[i];
			list = pd.fruitList.ToList();
			// Bonus points for FM who misleads players
			int misleadCounter = 0;
			listToMatch = players[currentGM].fruitList.ToList();

			// Bonus points for players who get perfect matches
			int perfectMatchCount = 0;

			for (int j = list.Count - 1; j >= 0; j--)
			{
				// Idea 2
				int score = 0;
				// We loop through all of GM's fruits
				for (int k = 0; k < listToMatch.Count; ++k)
				{
					// If we find a fruit that matches
					if (list[j] == listToMatch[k])
					{
						// Perfect match
						if (j == k)
						{
							score += playerPointMatchPerfect;
							++perfectMatchCount;
						}
						// Normal match
						else
						{
							score += playerPointMatch;
						}
					}
				}
				if (score == 0)
				{
					misleadCounter++;
				}
			}
			// Give the player bonus points for perfect matches
			if (perfectMatchCount == 2)
			{
				if(i == playerIdx)
				{
					return playerPoints2PerfectMatches;
				}
			}
			else if (perfectMatchCount == 3)
			{
				if (i == playerIdx)
				{
					return playerPoints3PerfectMatches;
				}
			}
			// Give the gm bonus points for misleads
			else if (misleadCounter == 3)
			{
				numMisled++;
			}
		}
		if(playerIdx == currentGM)
		{
			return numMisled * fmPointsMislead;
		}

		return 0;
	}

	public void CalculateEndTurn()
	{
		List<int> list;
		List<int> listToMatch;
		PlayerData pd;

		PlayerData fruitMaster = players[currentGM];
		fruitMaster.fruitScores = 
			new int[fruitMaster.fruitList.Count + 1].ToList();

		for(int i = 0; i < players.Count; ++i)
		{
			if (i == currentGM) continue;
			pd = players[i];
			list = pd.fruitList.ToList();
			listToMatch = players[currentGM].fruitList.ToList();

			// Fruit score + bonus
			pd.fruitScores = new int[list.Count + 1].ToList();

			// Bonus points for players who get perfect matches
			int perfectMatchCount = 0;
			// Bonus points for FM who misleads players
			int misleadCounter = 0;

			for(int j = list.Count - 1; j >= 0; j--)
			{
				// Idea 2
				int score = 0;
				// We loop through all of GM's fruits
				for(int k = 0; k < listToMatch.Count; ++k)
				{
					// If we find a fruit that matches
					if(list[j] == listToMatch[k])
					{
						// Perfect match
						if(j == k)
						{
							score += playerPointMatchPerfect;
							fruitMaster.fruitScores[k] += fmPointsLostMatch;
							++perfectMatchCount;
						}
						// Normal match
						else
						{
							score += playerPointMatch;
							fruitMaster.fruitScores[k] += fmPointsLostMatch;
						}
					}
				}
				if(score == 0)
				{
					misleadCounter++;
				}
				pd.fruitScores[j] = score;

				// Idea 1
// 				int score = 0;
// 				// Perfect Match
// 				if(list[j] == listToMatch[j])
// 				{
// 					score = playerPointMatchPerfect;
// 					fruitMaster.fruitScores[j] += fmPointsLostMatch;
// 					++perfectMatchCount;
// 					
// 				}
// 				// Match
// 				else if (listToMatch.Exists(x => x == list[j]))
// 				{
// 					score = playerPointMatch;
// 					fruitMaster.fruitScores[
// 						listToMatch.FindIndex(x => x == list[j])] 
// 						+= fmPointsLostMatch;
// 				}
// 				// Mislead
// 				else
// 				{
// 					++misleadCounter;
// 				}
// 				pd.fruitScores[j] = score;
			}
			// Give the player bonus points for perfect matches
			if(perfectMatchCount == 2)
			{
				pd.fruitScores[pd.fruitScores.Count - 1] 
					+= playerPoints2PerfectMatches;
			}
			else if (perfectMatchCount == 3)
			{
				pd.fruitScores[pd.fruitScores.Count - 1] 
					+= playerPoints3PerfectMatches;
			}
			// Give the gm bonus points for misleads
			else if (misleadCounter == 3)
			{
				fruitMaster.fruitScores[fruitMaster.fruitScores.Count - 1]
					+= fmPointsMislead;
			}

			// Total Score
			pd.earned = pd.GetPoints();
			pd.score += pd.earned;
		}

		pd = players[currentGM];
		pd.earned = 100 + pd.GetPoints();
		pd.score += pd.earned;
	}

	//List contains the same elements
	bool CheckLists(List<int> l1, List<int> l2)
	{
		l1.Sort();
		l2.Sort();
		return CheckListsPerfect(l1, l2);
	}

	// List is in the same order
	bool CheckListsPerfect(List<int> l1, List<int> l2)
	{
		for (int j = 0; j < l1.Count; ++j)
		{
			if (l1[j] != l2[j])
			{
				return false;
			}
		}
		return true;
	}
}
