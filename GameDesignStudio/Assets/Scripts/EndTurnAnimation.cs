﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndTurnAnimation : MonoBehaviour {

	public RectTransform lowerBanner;
	public RectTransform lowerBannerFold;
	public RectTransform hiddenText;

	public bool forwardAnimation;
	public bool backwardAnimation;

	private float currentTime;
	public float maxTime;
	private float pct;

	public float distanceThreshold;

	private Vector2 lowerBannerOffset;

	private Vector2 startPos;
	private Vector2 endPos;

	// The ref length we move the graphic down
	private const float LENGTHDOWN = -1162f/* / 1440f*/;

	// Debug
	private float dist;

	void Start () {
		// We do this to scale for any screen resolution
		lowerBannerOffset.y = /*Screen.height * */LENGTHDOWN;

		forwardAnimation = false;
		backwardAnimation = false;
	}
	
	void Update () {
		if(forwardAnimation)
		{
			currentTime += Time.deltaTime;
			UpdateTransforms();
			if (pct >= 1)
			{
				currentTime = maxTime;
				forwardAnimation = false;
			}
		}
		else if (backwardAnimation)
		{
			currentTime -= Time.deltaTime;
			UpdateTransforms();
			if (pct <= 0)
			{
				currentTime = 0;
				backwardAnimation = false;
			}
		}
	}

	public void StartDrag()
	{
#if UNITY_EDITOR
		startPos = Input.mousePosition;
#endif

		if (Input.touchCount > 0)
		{
			Touch touch = Input.GetTouch(0);
			startPos = touch.position;
		}
	}

	public void EndDrag()
	{
		// Only change when we are done
		if(forwardAnimation || backwardAnimation) return;

#if UNITY_EDITOR
		endPos = Input.mousePosition;
#endif

		if(Input.touchCount > 0)
		{
			Touch touch = Input.GetTouch(0);
			endPos = touch.position;
		}

		if(endPos == -Vector2.one)
		{
			Debug.Log("Fail");
			return;
		}

		dist = Vector2.Distance(startPos, endPos);
		if (dist > distanceThreshold)
		{
			float difference = endPos.y - startPos.y;
			if(difference < 0)
			{
				forwardAnimation = true;
				backwardAnimation = false;
			}
			else if(difference > 0)
			{
				forwardAnimation = false;
				backwardAnimation = true;
			}
		}
	}

	public void UpdateTransforms()
	{
		pct = currentTime / maxTime;

		lowerBannerFold.offsetMin = Vector2.Lerp(Vector2.zero,
			lowerBannerOffset, pct);
		lowerBannerFold.offsetMax = Vector2.Lerp(Vector2.zero,
			lowerBannerOffset, pct);

		lowerBanner.offsetMin = Vector2.Lerp(Vector2.zero,
			lowerBannerOffset, pct);

		hiddenText.offsetMin = Vector2.Lerp(
			-lowerBannerOffset, Vector2.zero, pct);
		hiddenText.offsetMax = Vector2.Lerp(
			-lowerBannerOffset, Vector2.zero, pct);
	}
}
