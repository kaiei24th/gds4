﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TruthReferences : MonoBehaviour {

	public GameManager gm;
	public GameObject nextButton;
	public Transform pin;
	public Transform postit;
	public Text screenText;
	public Text postitText;

	[Header("Reveal Text")]
	public float revealTextWaitTimer;
	public float revealTextTimer;
	private string chosenPrompt;

	[Header("Text Scale")]
	public AnimationCurve scaleCurve;
	public Vector3 endScaleVector;
	public float StartScale;
	public float scaleTime;

	[Header("Swing")]
	public AnimationCurve swingCurve;
	public float swingTime;
	public float swingSpeed;
	public float swingPower;

	void Start () {
		gm = FindObjectOfType<GameManager>();
		Debug.Assert(gm != null, "No GameManager");

		postitText.text = (gm.GetCurrentPlayer().isTruth) ? "TRUTH" : "LIE";
		chosenPrompt = gm.GetCurrentPlayer().choosenPrompt;

		CoroutineAnims.CoroutineHandler(CoroutineAnims.PrintString(
			revealTextWaitTimer, revealTextTimer, chosenPrompt, TypeText), 
			ShowTruth);
		CoroutineAnims.CoroutineHandler(CoroutineAnims.Wait(8f), EnableNext);
	}

	public void TypeText(string str)
	{
		screenText.text = str;
	}

	public void ScalePostit(Vector3 scale)
	{
		postit.localScale = scale;
	}

	public void EnablePostit(bool unusable)
	{
		postit.gameObject.SetActive(true);
	}

	public void ShowTruth(bool unused)
	{
		CoroutineAnims.CoroutineHandler(CoroutineAnims.Wait(0.6f), EnablePostit);
		CoroutineAnims.CoroutineHandler(CoroutineAnims.LerpVector3(0.5f,
			scaleTime, scaleCurve, endScaleVector * StartScale, 
			endScaleVector, ScalePostit), Swing);
	}

	public void UpdateRotation(float f)
	{
		postit.eulerAngles = new Vector3(0, 0, f);
	}

	public void Swing(bool unused)
	{
		CoroutineAnims.CoroutineHandler(CoroutineAnims.PendulumMotion(
			0f, swingTime, swingSpeed, swingPower, swingCurve, UpdateRotation));
	}

	public void EnableNext(bool unused)
	{
		nextButton.SetActive(true);
	}

	public void GoNext()
	{
		gm.SetNextPlayer(false);
		SceneCtrl.Load(SceneCtrl.Scenes.PassThePhone);
	}
}
