﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitTrigger : MonoBehaviour {

	public Transform otherTransform;

	public void OnTriggerEnter2D(Collider2D collision)
	{
		if(collision.gameObject.layer == 8)
		{
			Transform t = collision.transform;
			Vector3 newPos = t.position;
			newPos.x = otherTransform.position.x;
			t.position = newPos;
		}
	}

}
