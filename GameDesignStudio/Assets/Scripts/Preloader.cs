﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Preloader : MonoBehaviour {

	// id so we dont have duplicate preloaders
	public int id;

	void Start () {
		Preloader[] pl = FindObjectsOfType<Preloader>();
		foreach (Preloader p in pl)
		{
			if(p.id == id && p != this)
			{
				Destroy(gameObject);
				return;
			}
		}
		DontDestroyOnLoad(gameObject);
	}
}