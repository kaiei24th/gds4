﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugConfirmName : MonoBehaviour {

	public PlayerNameInputManager pnim;
	public LobbyManager lm;
	void Update()
	{
#if UNITY_EDITOR
		if (Input.GetKeyDown(KeyCode.Return))
		{
			if (pnim.confirmButton.interactable)
			{
				lm.IncrementCount();
				pnim.AddName();
			}
		}
#endif
	}

}
