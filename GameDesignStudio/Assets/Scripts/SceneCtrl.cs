﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneCtrl : MonoBehaviour {

	// 0 - Main Menu
	// 1 - Lobby
	// 2 - Settings
	// 3 - Player Turn
	// 4 - Waiting Turn

	public static int lastScene = -1;
	
	public enum Scenes
	{
		MainMenu		= 0,
		Lobby			= 1,
		Settings		= 2,
		// Customer Prompt | Pass The Phone | Side Reveal | Info Reveal
		// are all the same scene but with different info
		CustomerPrompt	= 3,
		PassThePhone	= 4,
		SideReveal		= 5,
		InfoReveal		= 6,
		PlayerTurn		= 7,
		EndRound		= 8,
		EndGame			= 9,

		GMIntro			= 10,
		PromptInstruct	= 11,
		PickFruit		= 12,
		ChoosePrompt	= 13,
		TwoTruthsOneLie	= 14,
		GameIntro		= 15,
		TruthResults	= 16,
		Discussion		= 17
	}

	// The int values 
	public static readonly Dictionary<Scenes, int> Level =
		new Dictionary<Scenes, int>
	{
		{ Scenes.MainMenu,			 0 },
		{ Scenes.Lobby,				 1 },
		{ Scenes.Settings,			 2 },
		{ Scenes.CustomerPrompt,	 3 },
		{ Scenes.PassThePhone,		 3 },
		{ Scenes.SideReveal,		 3 },
		{ Scenes.InfoReveal,		 3 },
		{ Scenes.PlayerTurn,		 4 },
		{ Scenes.EndRound,			 5 },
		{ Scenes.EndGame,			 6 },
		{ Scenes.GMIntro,			 3 },
		{ Scenes.GameIntro,			 3 },
		{ Scenes.PromptInstruct,	 3 },
		{ Scenes.PickFruit,			 7 },
		{ Scenes.ChoosePrompt,		 8 },
		{ Scenes.TwoTruthsOneLie,	 9 },
		{ Scenes.TruthResults,		10 },
		{ Scenes.Discussion,		11 }
	};

	public static void Load(Scenes idx)
    {
        SceneManager.LoadScene(Level[idx]);
		lastScene = Level[idx];
    }

	public static void Load(Scenes idx, LoadSceneMode loadSceneMode)
	{
		SceneManager.LoadScene(Level[idx], loadSceneMode);
		lastScene = Level[idx];
	}

	// Only for menu cos you u cant assign static functions to buttons
	// And cant use enums
	public void MenuLoad(int idx)
	{
		SceneManager.LoadScene(idx);
	}
}