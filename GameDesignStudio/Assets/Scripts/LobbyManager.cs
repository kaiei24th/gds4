﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LobbyManager : MonoBehaviour {

	public GameManager gm;
	//public GameObject playerNameInput;
	//public GameObject canvas;
	//public GameObject addPlayerButton;
	public List<PlayerInputManager> pim;
	public Button backGameButton;
	public Button startGameButton;

	public int minPlayers;
	public int maxPlayers;

	public int playerCount;

	// The max height for the player input
	[Range(0, 1)] public float maxHeight;
	// Height between each item
	[Range(0, 1)] public float betweenHeight;

	public List<PlayerData> players;
	//private List<NewPlayerReferences> playerReferences;

	void Start()
	{
		gm = FindObjectOfType<GameManager>();
		players = new List<PlayerData>();
		//playerReferences = new List<NewPlayerReferences>();
		//startGameButton.SetActive(false);
		startGameButton.interactable = false;
	}

	public void IncrementCount()
	{
		playerCount++;
	}

	public void DecrementCount()
	{
		playerCount--;
	}

	void Update()
	{
		if (playerCount >= minPlayers)
		{
			//startGameButton.SetActive(true);
			startGameButton.interactable = true;
		}
		else
		{
			startGameButton.interactable = false;
		}
		//if(players.Count != playerReferences.Count)
		//{
		//	Debug.Log("Player Input Mismatch");
		//}
		//else
		//{
		//	for(int i = 0; i < players.Count; ++i)
		//	{
		//		players[i].name = playerReferences[i].nameText.text;
		//	}
		//}
	}

// 	float CalculateHeight(int idx)
// 	{
// 		float height = Screen.height / 2;
// 		float heightPct = maxHeight - (idx * betweenHeight);
// 		return height * heightPct;
// 	}
// 
// 	void SetAnchors(RectTransform rt, int idx)
// 	{
// 		Vector2 anchorMin = rt.anchorMin;
// 		Vector2 anchorMax = rt.anchorMax;
// 
// 		float height = anchorMax.y - anchorMin.y;
// 
// 		float heightPct = maxHeight - (idx * betweenHeight);
// 		anchorMin.y = heightPct;
// 		anchorMax.y = heightPct + height;
// 
// 		rt.anchorMin = anchorMin;
// 		rt.anchorMax = anchorMax;
// 	}

	public void AddPlayer(string name)
	{
		int currentPlayer = players.Count;
		if (currentPlayer >= maxPlayers)
		{
			Debug.Log("Max Players Reached");
			//addPlayerButton.SetActive(false);
			return;
		}

		// Make new Player Data and item
// 		PlayerData newPlayer = new PlayerData {
// 			id = currentPlayer,
// 			name = name,
// 			gm = gm,
// 		};
		//var newGO = Instantiate(playerNameInput, canvas.transform) as GameObject;

		// Set the item transform
		//RectTransform rt = newGO.GetComponent<RectTransform>();
		//SetAnchors(rt, currentPlayer);

		// Set References
		//NewPlayerReferences npr = newGO.GetComponent<NewPlayerReferences>();
		//npr.idx = currentPlayer;
		//npr.playerIdx.text = (currentPlayer + 1).ToString();
		//npr.lm = this;

		// Add them to the list
		//players.Add(newPlayer);
		//playerReferences.Add(npr);

		if(players.Count >= minPlayers)
		{
			//startGameButton.SetActive(true);
			startGameButton.interactable = true;
		}
		if(players.Count >= maxPlayers)
		{
			//addPlayerButton.SetActive(false);
		}
	}

	public void RemovePlayer(int idx)
	{
		players.RemoveAt(idx);
		//Destroy(playerReferences[idx].gameObject);
		//playerReferences.RemoveAt(idx);

		// Move all the inputs upwards
		//if(idx < players.Count + 1)
		//{
		//	for(int i = idx; i < players.Count; ++i)
		//	{
		//		playerReferences[i].idx = i;
		//		playerReferences[i].playerIdx.text = (i + 1).ToString();
		//		RectTransform rt = playerReferences[i].GetComponent<RectTransform>();
		//		SetAnchors(rt, i);
		//	}
		//}

		// Re enable the add player button
		//addPlayerButton.SetActive(true);

		// Can't start without min players
		if(players.Count < minPlayers)
		{
			//startGameButton.SetActive(false);
			startGameButton.interactable = false;
		}
	}

	public void GoBack()
	{
		playerCount = 0;
		gm.LoadScene(SceneCtrl.Scenes.MainMenu);
	}

	public void AddAllPlayers()
	{
		foreach(var p in pim)
		{
			int currentPlayer = players.Count;
			PlayerData newP = new PlayerData
			{
				id = currentPlayer,
				name = p.playerName,
				gm = gm,
			};
			players.Add(newP);
		}
		StartGame();
	}

	public void StartGame()
	{
		// Probably unnecessary but we'll do it anyway
		if(playerCount < minPlayers)
		{
			Debug.Log("Not enough players");
			return;
		}
		if (gm.shufflePlayers)
		{
			gm.players = new List<PlayerData>();
			// We copy the original to avoid a log in the update method
			List<PlayerData> tempShuffle = new List<PlayerData>(players);

			// Randomly shuffle the players
			while(tempShuffle.Count > 0)
			{
				int idx = Random.Range(0, tempShuffle.Count);
				gm.players.Add(tempShuffle[idx]);
				tempShuffle.RemoveAt(idx);
			}
		}
		else
		{
			gm.players = players;
		}

		gm.StartGame();
		//gm.LoadScene(SceneCtrl.Scenes.CustomerPrompt);
		gm.LoadScene(SceneCtrl.Scenes.PassThePhone);
		//gm.LoadScene(SceneCtrl.Scenes.GMIntro);
	}
}
