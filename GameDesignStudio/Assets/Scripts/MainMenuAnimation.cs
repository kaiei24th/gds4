﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuAnimation : MonoBehaviour {

	public GameObject miniGame;
	public GameObject mainMenu;
	public GameObject settings;

	public RectTransform background1;
	public RectTransform background2;

	public RectTransform scaler;
	private bool forwardsAnimation = false;
	private bool backwardsAnimation = false;

	private Vector3 topPos;
	public Vector3 maxScale;

	private float currentTime;
	public float maxTime;

	private bool startBackwards;

	public void OnEnable()
	{
		if (SceneCtrl.lastScene != -1) startBackwards = true;
		else startBackwards = false;
	}

	void Start () {
		topPos = new Vector3(0f, Screen.height, 0f);
		if(startBackwards) BackwardsTextAnimation();
		else ForwardTextAnimation();
		forwardsAnimation = false;
	}
	
	void Update () {
		
		if(forwardsAnimation)
		{
			currentTime += Time.deltaTime;
			float pct = currentTime / maxTime;

			scaler.localScale = Vector3.Lerp(Vector3.one, maxScale, pct);
			background1.anchoredPosition3D = Vector3.Lerp(Vector3.zero, topPos, pct);
			background2.anchoredPosition3D = Vector3.Lerp(Vector3.zero, -topPos, pct);

			if(pct >= 1f)
			{
				forwardsAnimation = false;
			}
		}
		if(backwardsAnimation)
		{
			currentTime -= Time.deltaTime;
			float pct = currentTime / maxTime;

			scaler.localScale = Vector3.Lerp(Vector3.one, maxScale, pct);
			background1.anchoredPosition3D = Vector3.Lerp(Vector3.zero, topPos, pct);
			background2.anchoredPosition3D = Vector3.Lerp(Vector3.zero, -topPos, pct);

			if (pct <= 0f)
			{
				backwardsAnimation = false;
			}
		}
	}

	public void ForwardTextAnimation()
	{
		currentTime = 0;
		forwardsAnimation = true;
	}

	public void BackwardsTextAnimation()
	{
		currentTime = maxTime;
		backwardsAnimation = true;
	}

	public void LoadMainMenu()
	{
		StartCoroutine(ILoadMainMenu());
	}

	IEnumerator ILoadMainMenu()
	{
		yield return new WaitForSeconds(maxTime);
		mainMenu.SetActive(true);
		settings.SetActive(false);
	}

	public void LoadLobby()
	{
		StartCoroutine(ILoadLobby());
	}

	IEnumerator ILoadLobby()
	{
		yield return new WaitForSeconds(maxTime);
		SceneCtrl.Load(SceneCtrl.Scenes.Lobby);
	}

	public void LoadSettings()
	{
		settings.SetActive(true);
		StartCoroutine(ILoadSetttings());
	}

	IEnumerator ILoadSetttings()
	{
		yield return new WaitForSeconds(maxTime);
		mainMenu.SetActive(false);
	}
}
