﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomScrollRect : MonoBehaviour {

	public Scrollbar scrollbar;

	private Vector2 startPos;
	public Vector2 lastPos;
	public Vector2 pos;

	private Vector3 selfPos;

	public float yDelta;

	private float screenHeight;
	private float maxTopHeight;
	private float maxBotHeight;

	private RectTransform rect;

	private bool inertia;
	public float inertiaPower;
	public float inertiaFalloff;
	private float inertiaPowerLeft;

	const float maxHeight = 570f / 1440f;

	// Use this for initialization
	void Start () {
		inertia = false;
		screenHeight = Screen.height;
		rect = GetComponent<RectTransform>();

		maxTopHeight = 0f;
		maxBotHeight = maxHeight * screenHeight;
	}

	void Update()
	{
		if (Input.touchCount > 0 && inertia && inertiaPowerLeft < 1)
		{
			inertia = false;
		}

		if (inertia)
		{
			float inertiaDir = (pos.y - lastPos.y)/* > 0 ? 1f : -1f*/;
			
			Vector3 inertiaOffset = new Vector3(0f, 
				inertiaDir * inertiaPower * inertiaPowerLeft, 0f);

			inertiaPowerLeft -= Time.deltaTime * (1f + inertiaFalloff);

			rect.anchoredPosition3D += inertiaOffset;

			Vector3 clampValue = rect.anchoredPosition3D;
			clampValue.y = Mathf.Clamp(clampValue.y, maxTopHeight, maxBotHeight);

			rect.anchoredPosition3D = clampValue;

			float pct = (clampValue.y) / maxBotHeight;
			scrollbar.value = pct;

			if (inertiaPowerLeft <= 0)
			{
				inertia = false;
			}
		}
	}

	public void UpdateViaScrollbar(float val)
	{
		float pct = val * maxBotHeight;
		Vector3 newPos = rect.anchoredPosition3D;
		newPos.y = pct;
		rect.anchoredPosition3D = newPos;
	}

	public void BeginDrag()
	{
		inertia = false;
		Debug.Log("Start Drag");

		// Set the Vector to something invalid so we can check for it later
		startPos = -Vector2.one;

#if UNITY_EDITOR
		// Implement Mouse Controls
		startPos = Input.mousePosition;
#endif

		// Implement Touch Controls
		if (Input.touchCount > 0)
		{
			Touch touch = Input.GetTouch(0);
			startPos = touch.position;
		}

		// Check Failure
		if (startPos == -Vector2.one)
		{
			Debug.Log("Start Drag Fail");
		}

		selfPos = rect.anchoredPosition3D;
	}

	public void Dragging()
	{
		Debug.Log("Dragging");

		lastPos = pos;

		// Set the Vector to something invalid so we can check for it later
		pos = -Vector2.one;

#if UNITY_EDITOR
		// Implement Mouse Controls
		pos = Input.mousePosition;
#endif

		// Implement Touch Controls
		if (Input.touchCount > 0)
		{
			Touch touch = Input.GetTouch(0);
			pos = touch.position;
		}

		// Check Failure
		if (pos == -Vector2.one)
		{
			Debug.Log("Fail");
			return;
		}

		yDelta = pos.y - startPos.y;

		Vector3 offset = new Vector3(0, yDelta, 0);
		offset += selfPos;
		offset.y = Mathf.Clamp(offset.y, maxTopHeight, maxBotHeight);

		rect.anchoredPosition3D = offset;

		float pct = (offset.y) / maxBotHeight;
		scrollbar.value = pct;
	}

	public void EndDrag()
	{
		inertiaPowerLeft = 1;
		inertia = true;
	}

	public void DebugPress()
	{
		Debug.Log("Pressed");
	}
}
