﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;

public class EndRoundReferences : MonoBehaviour {

	public List<Sprite> fruitSprites;

	// 0 - GM banner
	// 1 - Winner banner
	public List<Sprite> achievementSprites;

	public List<EndTurnPlayerReferences> playersReferences;
	public List<EndTurnAnimation> animationReferences;

	public GameManager gm;

	public float startupTime;
	public float fruitDelayTime;

	public float fruitInitTime;
	public float fruitPopupTime;

	public float scoreWaitTimer;
	public float scoreTimer;
	public float scoreInbetweenWaitTimer;

	public AnimationCurve GMBannerCurve;
	public AnimationCurve fruitCurve;
	public AnimationCurve linearCurve;

	public GameObject nextButton;

	public const string format = "+#;-#;±0";

	private int fIdx = 0;

	// Use this for initialization
	void Start () {
		gm = FindObjectOfType<GameManager>();
		//gm.CalculateEndTurn();
		//gm.NewCalculateEndTurn();
		//UpdateRoundInfo();
		
		//UpdateSceneInfo();

		EmptySceneMode();
		StartAnimation();

		//UpdateRoundInfo();
	}

	void EmptySceneMode()
	{
		int i = 0;
		foreach (var etpr in playersReferences)
		{
			// Name
			etpr.playerName.text = gm.players[i].name;

			// Score
			etpr.points.text = "pt. 0";

			// Score Circle
			etpr.pointsCircle.fillAmount = 0f;

			// Fruit Images
			etpr.fruit1Fruit.sprite = fruitSprites[gm.players[i].fruitList[0]];
			etpr.fruit2Fruit.sprite = fruitSprites[gm.players[i].fruitList[1]];
			etpr.fruit3Fruit.sprite = fruitSprites[gm.players[i].fruitList[2]];

			etpr.fruit1Fruit.gameObject.transform.localScale = Vector3.zero;
			etpr.fruit2Fruit.gameObject.transform.localScale = Vector3.zero;
			etpr.fruit3Fruit.gameObject.transform.localScale = Vector3.zero;

			const string format = "±0";

			// Fruit Scores
			etpr.fruit1ScoreText.text = format;
			etpr.fruit2ScoreText.text = format;
			etpr.fruit3ScoreText.text = format;

			gm.players[i].fruitScores = new int[4].ToList();

			// Bonus
			etpr.bonusText.text = format;

			etpr.achievementImage.gameObject.transform.localScale = Vector3.zero;
			if (i == gm.currentGM)
			{
				etpr.achievementImage.sprite = achievementSprites[0];
			}
			else
			{
				etpr.achievementImage.sprite = null;
			}

			i++;
		}
	}

	void StartAnimation()
	{
		FruitMasterInit(false);
	}

	void FruitMasterInit(bool unused)
	{
		// FM Banner
		CoroutineAnims.CoroutineHandler(CoroutineAnims.LerpVector3(
			startupTime, fruitInitTime, GMBannerCurve, Vector3.zero,
			Vector3.one, playersReferences[gm.currentGM].
			UpdateAchievementImageScale),DisplayPlayerFruits);

		// Points text
		CoroutineAnims.CoroutineHandler(CoroutineAnims.LerpFloat(
			startupTime, fruitInitTime, linearCurve, 0f, 100f, 
			playersReferences[gm.currentGM].UpdatePointsText));

		// Points Circle
		CoroutineAnims.CoroutineHandler(CoroutineAnims.LerpFloat(
			startupTime, fruitInitTime, linearCurve, 0f, 1f, 
			playersReferences[gm.currentGM].UpdatePointsCircle));
	}

	void DisplayPlayerFruits(bool unused)
	{
		int i = 0;
		bool called = false;
		foreach (var etpr in playersReferences)
		{
			if (i == gm.currentGM)
			{
				i++;
				continue;
			}
			CoroutineAnims.CoroutineHandler(CoroutineAnims.LerpVector3(
				fruitDelayTime, fruitPopupTime, fruitCurve, Vector3.zero, 
				Vector3.one, etpr.UpdateFruit1Scale));
			CoroutineAnims.CoroutineHandler(CoroutineAnims.LerpVector3(
				fruitDelayTime + 0.1f, fruitPopupTime, fruitCurve, Vector3.zero, 
				Vector3.one, etpr.UpdateFruit2Scale));
			Task t = CoroutineAnims.CoroutineHandler(CoroutineAnims.LerpVector3(
				fruitDelayTime + 0.2f, fruitPopupTime, fruitCurve, Vector3.zero, 
				Vector3.one, etpr.UpdateFruit3Scale));

			if(!called)
			{
				t.Finished += UpdateAllScores;
				called = true;
			}
			i++;
		}
	}

	void DisplayGMFruit(bool unused)
	{
		CoroutineAnims.CoroutineHandler(CoroutineAnims.LerpVector3(
			fruitDelayTime, fruitPopupTime, fruitCurve, Vector3.zero, Vector3.one,
			playersReferences[gm.currentGM].fruitImageDelegates[fIdx]), UpdateFruitScore);
	}
	
	void UpdateFruitScore(bool unused)
	{
		//float waitTimer = 0.1f;
		//float timer = 1f;
		int fruitIdx = fIdx;
		fIdx++;
		int fruitID = gm.players[gm.currentGM].fruitList[fruitIdx];
		int i = 0;

		int totalValueLoss = 0;
		foreach (var player in gm.players)
		{
			if(i != gm.currentGM)
			{
				totalValueLoss += EachPlayer(i, fruitID, fruitIdx);
			}
			i++;
		}
		FMUpdate(fruitIdx, totalValueLoss);
	}

	int EachPlayer(int playerIdx, int FMFruitID, int FMFruitIdx)
	{
		int valueLoss = 0;
		PlayerData pd = gm.players[playerIdx];

		int previousScore = pd.GetPoints();

		for (int j = 0; j < pd.fruitList.Count; ++j)
		{
			int valueGain;
			if (pd.fruitList[j] == FMFruitID)
			{
				// Perfect Match
				if (j == FMFruitIdx)
				{
					valueGain = GameManager.playerPointMatchPerfect;
				}
				// Normal Match
				else
				{
					valueGain = GameManager.playerPointMatch;
				}

				// Update the Fruit Score
				CoroutineAnims.CoroutineHandler(
					CoroutineAnims.LerpFloat(scoreWaitTimer, scoreTimer,
					linearCurve, pd.fruitScores[j], pd.fruitScores[j] +
					valueGain, playersReferences[playerIdx].textDelegates[j]));

				pd.fruitScores[j] += valueGain;
				valueLoss += GameManager.fmPointsLostMatch;
			}
		}

		// Update the circle
		CoroutineAnims.CoroutineHandler(
			CoroutineAnims.LerpFloat(scoreWaitTimer, scoreTimer, 
			linearCurve, previousScore / 100f, (pd.GetPoints()) / 100f, 
			playersReferences[playerIdx].UpdatePointsCircle));

		// Update the maxFruitScore
		CoroutineAnims.CoroutineHandler(
			CoroutineAnims.LerpFloat(scoreWaitTimer, scoreTimer, 
			linearCurve, previousScore, (pd.GetPoints()),
			playersReferences[playerIdx].UpdatePointsText));

		return valueLoss;
	}

	void FMUpdate(int fruitIdx, int valueLoss)
	{
		PlayerData pd = gm.players[gm.currentGM];

		// Update fruit score
		CoroutineAnims.CoroutineHandler(
			CoroutineAnims.LerpFloat(0.5f, 1f, linearCurve, pd.fruitScores[fruitIdx],
			pd.fruitScores[fruitIdx] + valueLoss, playersReferences[gm.currentGM].
			textDelegates[fruitIdx]));

		int points = 100 + pd.GetPoints();
		int pointsEnd = points + valueLoss;

		// Update the circle
		CoroutineAnims.CoroutineHandler(
			CoroutineAnims.LerpFloat(scoreWaitTimer, scoreTimer, 
			linearCurve, points / 100f, pointsEnd / 100f,
			playersReferences[gm.currentGM].UpdatePointsCircle));

		// Update the maxFruitScore
		CoroutineAnims.CoroutineHandler(
			CoroutineAnims.LerpFloat(scoreWaitTimer, scoreTimer, 
			linearCurve, points, pointsEnd,
			playersReferences[gm.currentGM].UpdatePointsText));

		pd.fruitScores[fruitIdx] += valueLoss;
	}

	void UpdateAllScores(bool unused)
	{
		const int numFruits = 3;
		for(int i = 0; i < numFruits; ++i)
		{
			CoroutineAnims.CoroutineHandler(
				CoroutineAnims.Wait(i * scoreInbetweenWaitTimer),
				DisplayGMFruit);
		}

		CoroutineAnims.CoroutineHandler(
			CoroutineAnims.Wait((numFruits + 1) * 
			scoreInbetweenWaitTimer), UpdateBonus);
	}

	void UpdateBonus(bool unused)
	{
		int i = 0;
		foreach (var player in gm.players)
		{
			int points = gm.GetBonusPoints(i);
			if (points == 0)
			{
				i++;
				continue;
			}

			float gmBonus = (i == gm.currentGM) ? 100f : 0f;

			float pointsStart = (player.GetPoints() + gmBonus);
			float pointsEnd = (player.GetPoints() + points + gmBonus);

			// Update points text
			CoroutineAnims.CoroutineHandler(
				CoroutineAnims.LerpFloat(0f, scoreTimer, linearCurve,
				pointsStart, pointsEnd,
				playersReferences[i].UpdatePointsText));

			CoroutineAnims.CoroutineHandler(
				CoroutineAnims.LerpFloat(0f, scoreTimer, linearCurve,
				player.fruitScores[3], player.fruitScores[3] + points,
				playersReferences[i].UpdateBonusText));

			// Update Circle
			CoroutineAnims.CoroutineHandler(
				CoroutineAnims.LerpFloat(0f, scoreTimer,
				linearCurve, pointsStart / 100f, pointsEnd / 100f,
				playersReferences[i].UpdatePointsCircle));

			player.fruitScores[3] += points;

			i++;
		}
		CoroutineAnims.CoroutineHandler(
			CoroutineAnims.Wait(scoreTimer), SetWinner);
	}

	void SetWinner(bool unused)
	{
		List<int> scores = new int[4].ToList();
		int i = 0;
		foreach (var players in gm.players)
		{
			scores[i] = players.GetPoints();
			i++;
		}
		scores[gm.currentGM] = -100;

		int maxScore = scores.Max();
		var select = scores.Where(x => x == maxScore).ToList();
		if (select.Count() == 1)
		{
			int idx = scores.IndexOf(maxScore);
			playersReferences[idx]
				.achievementImage.sprite = achievementSprites[1];
			CoroutineAnims.CoroutineHandler(CoroutineAnims.LerpVector3(
				0f, fruitInitTime, GMBannerCurve, Vector3.zero,
				Vector3.one, playersReferences[idx].
				UpdateAchievementImageScale), AnimationEnd);
		}
		else
		{
			AnimationEnd(false);
		}
	}

	void OtherText(bool unused)
	{
		for(int i = 0; i < gm.players.Count; ++i)
		{
			CoroutineAnims.CoroutineHandler(
				CoroutineAnims.PrintString(
				0f, 2f, "Total Score: " + gm.players[i].score.ToString(), 
				playersReferences[i].UpdateHiddenText));
		}
	}

	void AnimationEnd(bool unused)
	{
		gm.CalculateEndTurn();
		UpdateSceneInfo();
		UpdateRoundInfo();
		nextButton.SetActive(true);
		CoroutineAnims.CoroutineHandler(CoroutineAnims.Wait(1f), OtherText);
	}

	void DoNothing(float unused)
	{

	}

	void UpdateSceneInfo()
	{
		int i = 0;
		List<int> scores = new int[playersReferences.Count].ToList();

		foreach (var etpr in playersReferences)
		{
			// Name
			etpr.playerName.text = gm.players[i].name;

			// Score
			scores[i] = gm.players[i].GetPoints();
			if (i == gm.currentGM) scores[i] += 100;
			etpr.points.text = "pt. " + scores[i].ToString();

			// Score Circle
			etpr.pointsCircle.fillAmount = (float)scores[i] / 100f;

			// Fruit Images
			etpr.fruit1Fruit.sprite = fruitSprites[gm.players[i].fruitList[0]];
			etpr.fruit2Fruit.sprite = fruitSprites[gm.players[i].fruitList[1]];
			etpr.fruit3Fruit.sprite = fruitSprites[gm.players[i].fruitList[2]];

			// Fruit Scores
			etpr.fruit1ScoreText.text = gm.players[i].fruitScores[0].ToString(format);
			etpr.fruit2ScoreText.text = gm.players[i].fruitScores[1].ToString(format);
			etpr.fruit3ScoreText.text = gm.players[i].fruitScores[2].ToString(format);

			// Bonus
			etpr.bonusText.text = gm.players[i].fruitScores[3].ToString(format);

			if(i == gm.currentGM)
			{
				etpr.achievementImage.sprite = achievementSprites[0];
			}

			i++;
		}

		// Make the gm's score so low they cant be chosen to win
		// Kinda hacky but there shouldn't be a situation where this fails
		// This is so we get the right index to change the sprites bcos if 
		// we delete the gm from the list we will get the wrong index
		scores[gm.currentGM] = -100;

		int maxScore = scores.Max();
		var select = scores.Where(x => x == maxScore).ToList();
		if (select.Count() == 1)
		{
			playersReferences[scores.IndexOf(maxScore)]
				.achievementImage.sprite = achievementSprites[1];
		}
	}
	
	void UpdateRoundInfo()
	{
		gm.currentGM = (gm.currentGM + 1) % gm.players.Count;
		gm.currentPlayer = gm.currentGM;
		gm.currentRound++;

		gm.choosenHints.Clear();
		gm.availableHints = gm.hints.ToList();
	}

	public void EndRound()
	{
		if (gm.currentRound == gm.maxRounds)
		{
			// Go to end game scene
			gm.LoadScene(SceneCtrl.Scenes.EndGame);
		}
		else
		{
			// Go to wait scene
			//gm.EndRound();
			MultiUseReference.called = 1;
			gm.LoadScene(SceneCtrl.Scenes.PassThePhone);
		}
	}
}
