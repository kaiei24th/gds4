﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerNameInputManager : MonoBehaviour {

	public InputField playerInput;
	public Button confirmButton;
	public PlayerInputManager pim;

	private void Start()
	{
		//playerInput.ActivateInputField();
	}

	public void AddName()
	{
		pim.UpdatePlayerInfo();
		playerInput.text = "";
		gameObject.SetActive(false);
	}

	public void CancelInput()
	{
		playerInput.text = "";
		gameObject.SetActive(false);
	}

	void Update()
	{
		confirmButton.interactable = playerInput.text.Length > 0 ? true : false;
	}
}
