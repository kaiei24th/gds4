﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DiscussionReferences : MonoBehaviour {

	public GameManager gm;
	public Text timerString;
	public GameObject instructions;
	public GameObject timesUp;

	public float debugSpeed;

	public float discussionTime;
	private float timer;

	private bool countDown;

	private float colorTime;
	private bool forwardsTween;

	private int min;
	private int sec;

	void Start () {
		gm = FindObjectOfType<GameManager>();
		timer = discussionTime;
		countDown = false;
	}
	
	void Update () {
		
		if(countDown)
		{
			timer -= Time.deltaTime * debugSpeed;
			min = ((int)timer / 60) % 60;
			sec = (int)timer % 60;
			timerString.text = min.ToString("D2") + ":" + sec.ToString("D2");

			if(timer < 30)
			{
				colorTime += Time.deltaTime * debugSpeed;
				if(forwardsTween) TweenColor(Color.white, Color.red, false);
				else TweenColor(Color.red, Color.white, true);
			}

			if(timer < 0)
			{
				StopTimer();
			}
		}
	}

	void TweenColor(Color start, Color end, bool setVal)
	{
		timerString.color = Color.Lerp(start, end, colorTime);
		if (timerString.color == end)
		{
			forwardsTween = setVal;
			colorTime = 0;
		}
	}

	void StopTimer()
	{
		countDown = false;
		timerString.gameObject.SetActive(false);
		timesUp.SetActive(true);
	}

	public void StartTimer()
	{
		countDown = true;
		instructions.SetActive(false);
		timerString.gameObject.SetActive(true);
		colorTime = 0;
	}

	public void GoNext()
	{
		gm.SetNextPlayer(false);
		SceneCtrl.Load(SceneCtrl.Scenes.PassThePhone);
	}
}
