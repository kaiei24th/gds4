﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewPhysicsFruit : MonoBehaviour {

	private Rigidbody2D rb2d;
	private Collider2D cc2d;

	public float minDensity;
	public float maxDensity;

	public float minScale;
	public float maxScale;

	private readonly float dragMultiplier = 1f;

	void Start () {
		cc2d = GetComponent<Collider2D>();
		rb2d = GetComponent<Rigidbody2D>();

		cc2d.density = Random.Range(minDensity, maxDensity);
		float randScale = Random.Range(minScale, maxScale);
		transform.localScale = new Vector3(randScale, randScale, randScale);

		StartCoroutine(StartDrag());
	}
	
	IEnumerator StartDrag()
	{
		yield return new WaitForSeconds(1);
		rb2d.drag = rb2d.mass * dragMultiplier;
	}
}
