﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TwoTruths : MonoBehaviour {

	public GameManager gm;

	public List<Sprite> fruitImages;

	public GameObject nextButton;

	[System.Serializable]
	private struct TwoTruthStruct
	{
		[SerializeField] public Image fruit;
		[SerializeField] public Text prompt;
		[SerializeField] public Toggle toggle;
	}

	[SerializeField] private List<TwoTruthStruct> references;

	void Start()
	{
		gm = FindObjectOfType<GameManager>();

		// Update Fruit Sprites & prompt text
		for (int i = 0; i < references.Count; ++i)
		{
			references[i].fruit.sprite =
				fruitImages[gm.players[gm.currentGM].fruitList[i]];
			references[i].prompt.text = gm.choosenHints[i];
		}
	}
	void Update()
	{
		int truths = 0;
		for (int i = 0; i < references.Count; ++i)
		{
			if (references[i].toggle.isOn)
			{
				++truths;
			}
		}

		nextButton.SetActive((truths == 2) ? true : false);
	}

	void UpdateTruths()
	{
		int j = 0;
		for(int i = 0; i < gm.players.Count; ++i)
		{
			if (i == gm.currentGM) continue;
			gm.players[i].isTruth = references[j].toggle.isOn;
			++j;
		}
	}

	public void GoNext()
	{
		UpdateTruths();
		gm.SetNextPlayer(false);
		SceneCtrl.Load(SceneCtrl.Scenes.PassThePhone);
	}
}