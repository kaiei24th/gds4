﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndTurnPlayerReferences : MonoBehaviour {

	public Text playerName;

	public Text points;
	public Image pointsCircle;

	[Space(5)]

	//public int fruit1Score;
	//public int fruit1Index;
	public Text fruit1ScoreText;
	public Image fruit1Fruit;
	private Transform fruit1Transform;

	[Space(5)]
	//public int fruit2Score;
	//public int fruit2Index;
	public Text fruit2ScoreText;
	public Image fruit2Fruit;
	private Transform fruit2Transform;

	[Space(5)]
	//public int fruit3Score;
	//public int fruit3Index;
	public Text fruit3ScoreText;
	public Image fruit3Fruit;
	private Transform fruit3Transform;

	[Space(5)]
	//public int bonusScore;
	public Text bonusText;
	public Image achievementImage;
	private Transform achievementTransform;
	public Text hiddenText;

	[HideInInspector] public List<System.Action<float>> textDelegates;
	[HideInInspector] public List<System.Action<Vector3>> fruitImageDelegates;

	public void Start()
	{
		achievementTransform = achievementImage.transform;
		fruit1Transform = fruit1Fruit.transform;
		fruit2Transform = fruit2Fruit.transform;
		fruit3Transform = fruit3Fruit.transform;
		textDelegates = new List<System.Action<float>>()
		{
			UpdateFruitText1,
			UpdateFruitText2,
			UpdateFruitText3,
		};
		fruitImageDelegates = new List<System.Action<Vector3>>()
		{
			UpdateFruit1Scale,
			UpdateFruit2Scale,
			UpdateFruit3Scale,
		};
	}

	public void UpdateHiddenText(string str)
	{
		hiddenText.text = "\nAdditional Info\n" + str;
	}

	public void UpdateBonusText(float x)
	{
		bonusText.text = x.ToString(EndRoundReferences.format);
	}

	public void UpdateFruitText1(float x)
	{
		fruit1ScoreText.text = x.ToString(EndRoundReferences.format);
	}

	public void UpdateFruitText2(float x)
	{
		fruit2ScoreText.text = x.ToString(EndRoundReferences.format);
	}

	public void UpdateFruitText3(float x)
	{
		fruit3ScoreText.text = x.ToString(EndRoundReferences.format);
	}

	public void UpdateAchievementImageScale(Vector3 x)
	{
		achievementTransform.localScale = x;
	}

	public void UpdatePointsText(float x)
	{
		points.text = "pt. " + (int)x;
	}

	public void UpdatePointsCircle(float x)
	{
		pointsCircle.fillAmount = Mathf.Max(0, x);
	}

	public void UpdateFruit1Scale(Vector3 x)
	{
		fruit1Transform.localScale = x;
	}

	public void UpdateFruit2Scale(Vector3 x)
	{
		fruit2Transform.localScale = x;
	}

	public void UpdateFruit3Scale(Vector3 x)
	{
		fruit3Transform.localScale = x;
	}
}
