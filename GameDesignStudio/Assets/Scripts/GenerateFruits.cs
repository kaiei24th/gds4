﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateFruits : MonoBehaviour {

	public Camera cam;

	public Transform waterParent;
	public List<GameObject> physicsPrefabs;

	public float currentCount;
	public float maxCount;

	public float currentCooldownTime;
	public float cooldownTime;

	void Start () {
		currentCooldownTime = 0;
	}
	
	void Update () {
		currentCount = waterParent.childCount;
		currentCooldownTime -= Time.deltaTime;
	}
	
	public void DestroyAllFruits()
	{
		foreach(Transform child in waterParent)
		{
			Destroy(child.gameObject);
		}
	}

	// Called from FruitTrigger Button
	public void AddPhysicsFruit()
	{
		if (currentCount >= maxCount) return;
		if (currentCooldownTime > 0) return;

		Vector2 pos = -Vector2.one;

#if UNITY_EDITOR
		//if (Input.GetMouseButton(0))
		//{
			pos = Input.mousePosition;
		//}
#endif

		if (Input.touchCount > 0)
		{
			Touch touch = Input.GetTouch(0);
			pos = touch.position;
		}

		if (pos == -Vector2.one) return;

		Vector3 worldPos = cam.ScreenToWorldPoint(pos);
		if (physicsPrefabs.Count > 0)
		{
			int fruitidx = Random.Range(0, physicsPrefabs.Count);
			worldPos.z = 0f;
			Instantiate(physicsPrefabs[fruitidx], worldPos,
				Quaternion.identity, waterParent);
			currentCount++;
			currentCooldownTime = cooldownTime;
		}
	}
}
