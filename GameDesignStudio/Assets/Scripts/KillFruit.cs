﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillFruit : MonoBehaviour {

	public float timeToShrink = 10f;
	public float totalShrinkTime = 1f;

	private bool shrink;
	private Vector3 startScale;
	private float currentshrinkTime;

	public void Start()
	{
		startScale = transform.localScale;
		currentshrinkTime = 0f;
		shrink = false;
		StartCoroutine(ShrinkFruit());
	}

	public void Update()
	{
		if (!shrink) return;

		currentshrinkTime += Time.deltaTime;
		float pct = currentshrinkTime / totalShrinkTime;
		transform.localScale = Vector3.Lerp(startScale, Vector3.zero, pct);

		if (pct >= 1)
		{
			Destroy(gameObject);
		}
	}

	IEnumerator ShrinkFruit()
	{
		yield return new WaitForSeconds(timeToShrink);
		shrink = true;
	}
}
