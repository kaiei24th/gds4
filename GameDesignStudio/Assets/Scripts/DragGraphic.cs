﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DragGraphic : MonoBehaviour {

	public Transform canvas;
	public int fruitID;

	public SelectFruitReferences sfr;
	private Image image;
	private RectTransform rect;
	private Vector2 startPos;
	private float width;
	private float height;

	//public Vector2 rawPos;
	public Vector2 pos;

	public void Start()
	{
		// Assume that this will be on the same gameobject as the canvas
		sfr = canvas.GetComponent<SelectFruitReferences>();
		image = GetComponent<Image>();
		rect = GetComponent<RectTransform>();
		startPos = rect.localPosition;
		width = (float)Screen.width;
		height = (float)Screen.height;
	}

	public void DragTheImage()
	{
		Debug.Log("Dragging");

		// Set the Vector to something invalid so we can check for it later
		pos = -Vector2.one;

#if UNITY_EDITOR
		// Implement Mouse Controls
		//rawPos = Input.mousePosition;
		pos = Input.mousePosition;
#endif

		// Implement Touch Controls
		if(Input.touchCount > 0)
		{
			Touch touch = Input.GetTouch(0);
			//rawPos = touch.position;
			pos = touch.position;
		}

		// Check Failure
		if(pos == -Vector2.one)
		{
			Debug.Log("Fail");
			return;
		}

		// Move the Graphic
		// The position given to us is relative to the top left corner
		// but local poisition is relative to the middle so we convert it
		pos.x -= width / 2f;
		pos.y -= height / 2f;
		rect.localPosition = pos;

		// Move the Anchors
		AnchorToCorners();
	}

	public void EndDrag()
	{
		Debug.Log("End Drag");
		foreach(RectTransform slot in sfr.slots)
		{
			// Hacky methods in which we calculate the actual position of the
			// rectangle that for some reason unity doesnt provide an actual
			// way to calculate
			// 
			// We just calculate the slots rectangle in which it will have a 
			// local position, then we add all the parents positions up until
			// the canvas gameobject to get actual position of the rectangle in
			// screen space
			// 
			// This version only works when the Panel gameobject is a child of
			// the canvas
// 			Rect r = slot.rect;
// 			Vector3 p1 = slot.parent.localPosition;
// 			Vector3 p2 = slot.parent.parent.localPosition;
// 			r.center += new Vector2(p1.x, p1.y);
// 			r.center += new Vector2(p2.x, p2.y);

			// This version should work no matter how many parent objects there
			// are
			Rect r = GetScreenRect(slot);

			if (r.Contains(pos))
			{
				sfr.UpdateSlot(slot, image, fruitID);
				//slot.GetComponent<DropFruit>().UpdateImage(image);
				//Debug.Log(slot.gameObject.name);
				break;
			}
		}

		rect.localPosition = startPos;
		AnchorToCorners();
	}

	public Rect GetScreenRect(RectTransform rect)
	{
		Rect r = rect.rect;

		RectTransform currentRect = rect;
		while(currentRect.parent.name != canvas.name)
		{
			Vector3 pos = currentRect.parent.localPosition;
			r.center += new Vector2(pos.x, pos.y);
			currentRect = currentRect.parent as RectTransform;
		}
		return r;
	}

	public void AnchorToCorners()
	{
		RectTransform t = rect;
		RectTransform pt = t.parent as RectTransform;

		if (t == null || pt == null) return;

		Vector2 newAnchorsMin = new Vector2(t.anchorMin.x + t.offsetMin.x / pt.rect.width,
											t.anchorMin.y + t.offsetMin.y / pt.rect.height);
		Vector2 newAnchorsMax = new Vector2(t.anchorMax.x + t.offsetMax.x / pt.rect.width,
											t.anchorMax.y + t.offsetMax.y / pt.rect.height);

		t.anchorMin = newAnchorsMin;
		t.anchorMax = newAnchorsMax;
		t.offsetMin = t.offsetMax = new Vector2(0, 0);
	}
}
