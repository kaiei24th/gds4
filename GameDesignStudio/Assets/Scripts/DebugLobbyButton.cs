﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugLobbyButton : MonoBehaviour {

	public List<string> playerNames;

	public LobbyManager lm;

	public void DebugAddPlayerNames()
	{
		foreach (string name in playerNames)
		{
			int currentPlayer = lm.players.Count;
			PlayerData newP = new PlayerData
			{
				id = currentPlayer,
				name = name,
				gm = lm.gm,
			};
			lm.players.Add(newP);
		}
		lm.playerCount = lm.minPlayers;
		lm.StartGame();
	}

}
