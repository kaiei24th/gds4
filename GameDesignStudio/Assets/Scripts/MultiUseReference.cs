﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MultiUseReference : MonoBehaviour {

	public GameManager gm;

	public Text screenText;
	//public Text buttonText;
	public Text cornerText;
	public string fruitmasterString;
	public string gameString;
	public string passphoneString;
	public string promptString;
	public string trueliesString;

	public static int currentValue = 1;
	public static int called = 1;

	private SceneCtrl.Scenes gameState;

	void Start()
	{
		gm = FindObjectOfType<GameManager>();
		Debug.Assert(gm != null, "No GameManager");
		gameState = gm.gameState;
		NewUpdateSceneInfo();
	}

	void PassPhone(PlayerData pd)
	{
		screenText.text = passphoneString + ' ' + pd.name;
		cornerText.text = "pass phone";
	}

	void FruitMaster(PlayerData pd)
	{
		screenText.text = fruitmasterString;
		cornerText.text = "fruit master";
	}

	void GameIntro(PlayerData pd)
	{
		screenText.text = gameString;
		cornerText.text = "instructions";
	}

	void PromptInstructions(PlayerData pd)
	{
		screenText.text = promptString;
		cornerText.text = "instructions";
	}
// 
// 	void TruthResults(PlayerData pd)
// 	{
// 		screenText.text = "Your prompt\n" + pd.choosenPrompt +
// 			((pd.isTruth) ? "\nis the truth" : "\nis a lie");
// 		cornerText.text = "the truth";
// 	}

	// What shows on the screen
	void NewUpdateSceneInfo()
	{
		PlayerData pd = gm.GetCurrentPlayer();

		switch(called)
		{
			case  1: // Pass phone (FM)
				PassPhone(pd);
				break;
			case  2: // GM Intro
				FruitMaster(pd);
				break;
			case  3: // Game Intro
				GameIntro(pd);
				break;
			case  4: // Prompt Instructions
				PromptInstructions(pd);
				break;
			// Choose Prompt
			case  5: // Pass phone (P1)
			case  6: // Pass phone (P2)
			case  7: // Pass phone (P3)
			// 2 Truths 1 Lie
			case  8: // Pass phone (FM)
			
			// Truth Reveal
			case  9: // Pass phone (P1)
			case 10: // Pass phone (P2)
			case 11: // Pass phone (P3)
				PassPhone(pd);
				break;
			case 12: // Pass phone (P1)
			case 13: // Pass phone (P2)
			case 14: // Pass phone (P3)
			case 15: // Pass phone (FM)
			case 16: // Pass phone (FM)
				PassPhone(pd);
				break;
		}
		Debug.Log(called + " " + gameState);
	}

	void UpdateSceneInfo()
	{
		PlayerData pd = gm.GetCurrentPlayer();
		Debug.Log(called++ + " " + gameState);

		switch (gameState)
		{
			case SceneCtrl.Scenes.GMIntro:
				screenText.text = fruitmasterString;
				cornerText.text = "fruit master";
				//buttonText.text = "Start";
				break;
			case SceneCtrl.Scenes.GameIntro:
				screenText.text = gameString;
				cornerText.text = "instructions";
				//buttonText.text = "Next";
				break;
// 			case SceneCtrl.Scenes.CustomerPrompt:
// 				// Pull the customer prompt from game manager
// 				screenText.text = gm.GetCurrentPrompt().GetRequirement(gm);
// 				screenText.text += "\n\nThere are " + gm.numSabotagers + " sabotagers";
// 				buttonText.text = "Start";
// 				break;
			case SceneCtrl.Scenes.PassThePhone:
				screenText.text = passphoneString + ' ' + pd.name;
				cornerText.text = "pass phone";
				//buttonText.text = "Start Turn";
				break;
			case SceneCtrl.Scenes.PromptInstruct:
				screenText.text = promptString;
				cornerText.text = "instructions";
				//buttonText.text = "Next";
				break;
			case SceneCtrl.Scenes.TwoTruthsOneLie:
				screenText.text = trueliesString;
				cornerText.text = "truths & lie";
				foreach (string hint in gm.choosenHints)
				{
					screenText.text += "\n" + hint;
				}
				screenText.text += "\nYou chose ";
				foreach(int i in gm.players[gm.currentGM].fruitList)
				{
					screenText.text += gm.GetCurrentDeck().cards[i] + "  ";
				}
				//buttonText.text = "Next";
				break;
// 			case SceneCtrl.Scenes.SideReveal:
// 				// Side
// 				List<string> bad = gm.GetSabotagers();
// 				screenText.text = "You are " + 
// 					((pd.side == PlayerData.Side.Mixers) ?
// 					"good\n(Fulfill the order)" : 
// 					"up to no good\n(Sabotage the order)");
// 				if (pd.side == PlayerData.Side.Sabotagers)
// 				{
// 					if(bad.Count > 0)
// 					{
// 						screenText.text += "\nYour partners in crime are:";
// 						if (bad.Count > 1)
// 						{
// 							for(int i = 0; i < bad.Count - 1; ++i)
// 							{
// 								screenText.text += " " + bad[i] + ",";
// 							}
// 							screenText.text = screenText.text.Remove(screenText.text.Length - 1);
// 							screenText.text += " and " + bad[bad.Count - 1];
// 						}
// 						else
// 						{
// 							screenText.text += " " + bad[0];
// 						}
// 					}
// 					else
// 					{
// 						screenText.text += "\nYou are a lone wolf";
// 					}
// 				}
// 				buttonText.text = "Choose Fruit";
// 				break;
// 			case SceneCtrl.Scenes.InfoReveal:
// 				screenText.text = "Choose to reveal what you know";
// 				buttonText.text = "Next Turn";
// 				break;
		}
	}

	public void NewChangeScene()
	{
		switch(called)
		{
			case 1: // Pass Phone (FM) >=> FM Intro
				gm.LoadScene(SceneCtrl.Scenes.GMIntro);
				break;
			case 2: // FM Intro >=> Game Intro
				gm.LoadScene(SceneCtrl.Scenes.GameIntro);
				break;
			case 3: // Game Intro >=> Pick Fruit (FM) >=> Prompt Instructions
				gm.LoadScene(SceneCtrl.Scenes.PickFruit);
				break;
			case 4: // Prompt Instructions >=> Pass Phone (P1)
				gm.SetNextPlayer(false);
				gm.LoadScene(SceneCtrl.Scenes.PassThePhone);
				break;
			case 5: // Pass Phone (P1) >=> Choose Prompt (P1) >=> Pass Phone (P2)
			case 6: // Pass Phone (P2) >=> Choose Prompt (P2) >=> Pass Phone (P3)
			case 7: // Pass Phone (P3) >=> Choose Prompt (P3) >=> Pass Phone (FM)
				gm.LoadScene(SceneCtrl.Scenes.ChoosePrompt);
				break;
			case 8: // Pass Phone (FM) >=> 2 Truths 1 Lie (FM) >=> Pass Phone (P1)
				gm.LoadScene(SceneCtrl.Scenes.TwoTruthsOneLie);
				break;
			case 9:  // Pass Phone (P1) >=> Truth Results (P1) >=> Pass Phone (P2)
			case 10: // Pass Phone (P2) >=> Truth Results (P2) >=> Pass Phone (P3)
			case 11: // Pass Phone (P3) >=> Truth Results (P3) >=> Discussion
				//gm.SetNextPlayer(false);
				gm.LoadScene(SceneCtrl.Scenes.TruthResults);
				break;
			case 12: // Discussion >=> Pass Phone (P1)
				gm.LoadScene(SceneCtrl.Scenes.Discussion);
				break;
			case 13: // Pass Phone (P1) >=> Pick Fruit (P1) >=> Pass Phone (P2)
			case 14: // Pass Phone (P2) >=> Pick Fruit (P2) >=> Pass Phone (P3)
			case 15: // Pass Phone (P3) >=> Pick Fruit (P3) >=> Pass Phone (FM)
				gm.LoadScene(SceneCtrl.Scenes.PickFruit);
				break;
			case 16: // Pass Phone (FM) >=> Results
				gm.LoadScene(SceneCtrl.Scenes.EndRound);
				break;
		}

		called++;
	}

	public void ChangeScene()
	{
		NewChangeScene();
		return;
		switch(gameState)
		{
// 			case SceneCtrl.Scenes.CustomerPrompt:
// 				gm.LoadScene(SceneCtrl.Scenes.PassThePhone);
// 				break;
			case SceneCtrl.Scenes.PassThePhone:
				// Go to GM intro scene
				if(/*gm.currentPlayer == gm.currentGM &&*/ 
					(gm.lastGameState == SceneCtrl.Scenes.Lobby ||
					gm.lastGameState == SceneCtrl.Scenes.EndRound))
				{
					gm.LoadScene(SceneCtrl.Scenes.GMIntro);
				}
				// Choose prompt phase
				else if(gm.currentPlayer != gm.currentGM &&
					(gm.lastGameState == SceneCtrl.Scenes.PromptInstruct || 
					gm.lastGameState == SceneCtrl.Scenes.ChoosePrompt))
				{
					gm.LoadScene(SceneCtrl.Scenes.ChoosePrompt);
				}
				// 2 Truths 1 Lie Phase
				else if(gm.currentPlayer == gm.currentGM &&
					gm.lastGameState == SceneCtrl.Scenes.ChoosePrompt)
				{
					gm.LoadScene(SceneCtrl.Scenes.TwoTruthsOneLie);
				}
				// Player Pick Fruit Phase
				else if(gm.currentPlayer != gm.currentGM)
				{
					gm.LoadScene(SceneCtrl.Scenes.PickFruit);
				}
				else
				{
					gm.LoadScene(SceneCtrl.Scenes.EndRound);
					if (gm.currentRound > gm.maxRounds)
					{
						gm.LoadScene(SceneCtrl.Scenes.EndGame);
					}
					else
					{
						
					}
				}
				break;
			case SceneCtrl.Scenes.GMIntro:
				gm.LoadScene(SceneCtrl.Scenes.GameIntro);
				break;
			case SceneCtrl.Scenes.GameIntro:
				gm.LoadScene(SceneCtrl.Scenes.PickFruit);
				break;
			case SceneCtrl.Scenes.PromptInstruct:
				gm.SetNextPlayer(false);
				gm.LoadScene(SceneCtrl.Scenes.PassThePhone);
				break;
			case SceneCtrl.Scenes.TwoTruthsOneLie:
				gm.SetNextPlayer(false);
				gm.LoadScene(SceneCtrl.Scenes.PassThePhone);
				break;
// 			case SceneCtrl.Scenes.SideReveal:
// 				gm.LoadScene(SceneCtrl.Scenes.PlayerTurn);
// 				break;
// 			case SceneCtrl.Scenes.InfoReveal:
// 				gm.LoadScene(SceneCtrl.Scenes.PassThePhone);
// 				break;
		}
	}
}