﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class SelectFruitReferences : MonoBehaviour {

	public GameManager gm;
	public int[] fruitIDs;
	public List<RectTransform> slots;
	public GameObject nextButton;

	void Start () {
		gm = FindObjectOfType<GameManager>();
		fruitIDs = new int[slots.Count];
		for(int i = 0; i < slots.Count; ++i)
		{
			fruitIDs[i] = -1;
		}
	}

	public void UpdateSlot(RectTransform slot, Image newImage, int fruitID)
	{
		slot.GetComponent<DropFruit>().UpdateImage(newImage);
		int idx = slots.IndexOf(slot);
		ChangeValues(idx, fruitID);
	}

	public void RemoveSlot(RectTransform slot)
	{
		int idx = slots.IndexOf(slot);
		ChangeValues(idx, -1);
	}

	void ChangeValues(int idx, int val)
	{
		if (idx >= 0 && idx < slots.Count)
		{
			fruitIDs[idx] = val;
		}
		else
		{
			Debug.Log("fruitID not set");
		}

		foreach (int id in fruitIDs)
		{
			if (id == -1)
			{
				nextButton.SetActive(false);
				return;
			}
		}

		nextButton.SetActive(true);
	}

	public void NextPart()
	{
		gm.GetCurrentPlayer().fruitList = fruitIDs.ToList();
		if(gm.currentPlayer == gm.currentGM)
		{
			gm.LoadScene(SceneCtrl.Scenes.PromptInstruct);
		}
		else
		{
			if (gm.SetNextPlayer(false))
			{
				// Last player
				gm.LoadScene(SceneCtrl.Scenes.PassThePhone);
			}
			else
			{
				// Next player
				gm.LoadScene(SceneCtrl.Scenes.PassThePhone);
			}
		}
	}
}