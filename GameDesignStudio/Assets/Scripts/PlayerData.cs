﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[System.Serializable]
public class PlayerData
{
// 	public enum Side
// 	{
// 		Mixers = 0, // Mixers
// 		Sabotagers = 1  // Sabotagers
// 	}

	[SerializeField] public int id;
	[SerializeField] public string name;

	// Allegiance this round
	//[SerializeField] public Side side;
	// Item chosen that round
	//[SerializeField] public int choosenItem;
	// Total Score
	[SerializeField] public float score;
	// Score earned that round
	[SerializeField] public float earned;

	//[SerializeField] public int maxLogs;
	// Round Number instead of time cos its better
	//[SerializeField] public List<int> timestamp;
	//[SerializeField] public List<string> log;

	[SerializeField] public List<int> fruitList;
	[SerializeField] public List<int> fruitScores;

	//// Skills it can use
	//[SerializeField] public List<SkillBase> skillStorage;
	//
	//// Skills applied on this players turn
	//[SerializeField] public List<SkillBase> skillsApplied;

	[SerializeField] public string choosenPrompt;
	[SerializeField] public bool isTruth;

	// So we can poll the round number
	public GameManager gm;

	// For generating new id's
	private static int currentID = 0;

	private int GenerateID()
	{
		return currentID++;
	}

	public PlayerData()
	{
		id = GenerateID();
		name = "";
		//choosenItem = -1;
		score = 0;

		//maxLogs = 10;
		//timestamp = new List<int>(10);
		//log = new List<string>(10);

		//skillStorage = new List<SkillBase>();
		//skillsApplied = new List<SkillBase>();
	}

	public int GetPoints()
	{
		return fruitScores.Sum(x => x);
	}

// 	public void AddLog(string str)
// 	{
// 		AddLog(str, gm.currentRound);
// 	}
// 
// 	public void AddLog(string str, int roundNum)
// 	{
// 		timestamp.Add(roundNum);
// 		log.Add(str);
// 		if (log.Count > maxLogs)
// 		{
// 			log.RemoveAt(0);
// 			timestamp.RemoveAt(0);
// 		}
// 	}
}