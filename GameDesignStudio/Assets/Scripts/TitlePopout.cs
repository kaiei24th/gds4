﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitlePopout : MonoBehaviour {

	private RectTransform rect;

	public AnimationCurve curve;
	public float waitTime;
	public float animTime;
	public Vector2 offset;

	void Start () {
		rect = GetComponent<RectTransform>();
		CoroutineAnims.CoroutineHandler(CoroutineAnims.LerpVector2(waitTime,
			animTime, curve, offset, Vector2.zero, TitlePopIn));
	}

	public void TitlePopIn(Vector2 v)
	{
		rect.offsetMin = v;
		rect.offsetMax = v;
	}
}
