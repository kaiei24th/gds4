﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DropFruit : MonoBehaviour {

	private Image image;
	private Button button;
	public SelectFruitReferences sfr;

	void Start () {
		image = GetComponent<Image>();
		button = GetComponent<Button>();
		button.interactable = false;
	}

	public void UpdateImage(Image newImage)
	{
		image.sprite = newImage.sprite;
		image.type = Image.Type.Simple;
		image.preserveAspect = true;
		button.interactable = true;
	}

	public void RemoveImage()
	{
		sfr.RemoveSlot(GetComponent<RectTransform>());
		image.sprite = null;
		button.interactable = false;
	}
}
