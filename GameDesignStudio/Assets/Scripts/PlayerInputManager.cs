﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInputManager : MonoBehaviour {

	public string playerName;

	public Image playerImage;
	public Button playerButton;
	public Sprite playerSprite;
	public Sprite startSprite;
	public Text playerText;
	public GameObject playerNameInput;
	public GameObject deleteButton;

	public PlayerNameInputManager pnim;

	public void UnhidePlayerNameInput()
	{
		playerNameInput.SetActive(true);
	}

	public void UnhideDeleteButton()
	{
		deleteButton.SetActive(true);
	}

	public void GetPlayerName()
	{
		pnim.pim = this;
		UnhidePlayerNameInput();
	}

	public void UpdatePlayerInfo()
	{
		playerName = pnim.playerInput.text;
		playerText.text = playerName;
		playerImage.sprite = playerSprite;
		playerButton.enabled = false;
		UnhideDeleteButton();
	}

	public void ResetPlayer()
	{
		playerName = null;
		playerText.text = playerName;
		playerImage.sprite = startSprite;
		playerButton.enabled = true;
		deleteButton.SetActive(false);
		
	}
}
