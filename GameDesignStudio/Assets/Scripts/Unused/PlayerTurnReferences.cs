﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerTurnReferences : MonoBehaviour {

// 	public GameObject EndTurnButton;
// 	
// 	// Player Info
// 	public Text PlayerNoText;
// 	public Text PlayerNameText;
// 	public Text RoundNoText;
// 	public Text ScoreNoText;
// 	public Text LogText;
// 	public Text promptText;
// 
// 	// Skills
// 	public int selectedSkill;
// 	public Text skillName;
// 	public Text skillDescription;
// 	public GameObject skillEnable;
// 	public GameObject[] skillTargets;
// 	public GameObject skillPrevious;
// 	public GameObject skillNext;
// 
// 	public GameObject TimerFill;
// 	public GameObject[] fruits;
// 
// 	public GameManager gm;
// 	public PlayerData pd;
// 
// 	//private RectTransform bar;
// 
// 	void Start() {
// 		EndTurnButton.SetActive(false);
// 		gm = FindObjectOfType<GameManager>();
// 		UpdatePlayerInfo();
// 
// 		PromptBase pb = gm.GetCurrentPrompt();
// 		string s = (pd.side == PlayerData.Side.Mixers) ? "Mixer" : "Sabotager";
// 		promptText.text = pb.GetRequirement(gm) + "\n\nRole: " + s + "\n\nSatisfaction Level: " 
// 			+ (pb.complete * 100.0f).ToString() + "%";
// 		gm.ptr = this;
// 		gm.turnTime = 0;
// // 		gm.ActivateSkills(gm.currentPlayer, gm.currentPlayer, 
// // 			SkillBase.ApplicationType.DuringTurn);
// 
// 		bool noFruits = true;
// 		foreach(GameObject go in fruits)
// 		{
// 			if(go.activeSelf)
// 			{
// 				noFruits = false;
// 				break;
// 			}
// 		}
// 
// 		if(noFruits)
// 		{
// 			gm.GetCurrentPlayer().AddLog("Oh no you have no fruits to pick... YIKES");
// 			EndTurnButton.SetActive(true);
// 		}
// 
// // 		// Update Skills
// // 		for(int i = 0; i < gm.players.Count; ++i)
// // 		{
// // 			SkillSelectReferences ssr = skillTargets[i].
// // 				GetComponent<SkillSelectReferences>();
// // 			ssr.playerName.text = gm.players[i].name;
// // 		}
// 
// 		foreach (GameObject go in skillTargets)
// 		{
// 			go.SetActive(false);
// 		}
// 
// 		//UpdateSkillInfo();
// 	}
// 	
// 	void UpdatePlayerInfo()
// 	{
// 		pd = gm.GetCurrentPlayer();
// 
// 		PlayerNoText.text = "P" + (gm.currentPlayer + 1).ToString();
// 		PlayerNameText.text = pd.name;
// 		RoundNoText.text = gm.currentRound.ToString();
// 		ScoreNoText.text = ((int)(pd.score)).ToString();
// 
// 		//bar = TimerFill.GetComponent<RectTransform>();
// 	}
// 
// 	void Update() {
// 		if(gm)
// 		{
// 			// Update the log
// 			if(pd.log.Count != 0)
// 			{
// 				int idx = pd.log.Count - 1;
// 				LogText.text = "Round " + pd.timestamp[idx].ToString() + ": " + pd.log[idx];
// 			}
// 			else
// 			{
// 				LogText.text = "";
// 			}
// 
// 			// Update the timer
// // 			float timePct = gm.turnTime / gm.turnTimer;
// // 			{
// // 				Vector3 scale = bar.localScale;
// // 				scale.y = timePct;
// // 				bar.localScale = scale;
// // 			}
// 
// 			// We need to change it more subtlety
// // 			if(timePct >= 1)
// // 			{	
// // 				EndTurn();
// // 			}
// 		}
// 
// 	}
// 
// 	public void UpdateSelection(int selection)
// 	{
// 		gm.UpdateSelection(selection);
// 		EndTurnButton.SetActive(true);
// 	}
// 
// 	public void EndTurn()
// 	{
// //		// Enable Skills
// // 		int target = GetTarget();
// // 		if(target != -1)
// // 		{
// // 			gm.EnableSkills(selectedSkill, target);
// // 		}
// 
// 		gm.GetCurrentPrompt().AddFruit(gm, pd.choosenItem);
// 
// 		gm.EndTurn();
// 	}
// 
// // 	public void UpdateSkillInfo()
// // 	{
// // 		if(pd.skillStorage.Count == 0)
// // 		{
// // 			skillName.text = "No Skills";
// // 			skillDescription.text = "";
// // 			skillEnable.SetActive(false);
// // 			skillEnable.GetComponent<Toggle>().isOn = false;
// // 			skillPrevious.SetActive(false);
// // 			skillNext.SetActive(false);
// // 
// // 			for (int i = 0; i < gm.players.Count; ++i)
// // 			{
// // 				skillTargets[i].SetActive(false);
// // 			}
// // 		}
// // 		else
// // 		{
// // 			selectedSkill = 0;
// // 			skillName.text = pd.skillStorage[selectedSkill].name;
// // 			skillDescription.text = pd.skillStorage[selectedSkill].description;
// // 			skillEnable.SetActive(true);
// // 			skillEnable.GetComponent<Toggle>().isOn = false;
// // 			
// // 			skillPrevious.SetActive((selectedSkill > 1) ? true : false);
// // 			skillNext.SetActive((selectedSkill < pd.skillStorage.Count - 1) 
// // 				? true : false);
// // 
// // 			for(int i = 0; i < gm.players.Count; ++i)
// // 			{
// // 				skillTargets[i].SetActive(pd.skillStorage[selectedSkill].
// // 					CanBeActivatedOn(gm, gm.currentPlayer, i));
// // 			}
// // 		}
// // 	}
// // 
// // 	public void ChangeSkill(int i)
// // 	{
// // 		selectedSkill += i;
// // 		UpdateSkillInfo();
// // 	}
// 
// 	int GetTarget()
// 	{
// 		for (int i = 0; i < skillTargets.Length; ++i)
// 		{
// 			if (skillTargets[i].GetComponent<Toggle>().isOn)
// 				return i;
// 		}
// 		return -1;
// 	}
// 
// 	// Turn off all the other toggles
// 	public void UpdateToggles(int j)
// 	{
// 		for (int i = 0; i < skillTargets.Length; ++i)
// 		{
// 			if(i == j) continue;
// 			skillTargets[i].GetComponent<Toggle>().isOn = false;
// 		}
// 	}
}