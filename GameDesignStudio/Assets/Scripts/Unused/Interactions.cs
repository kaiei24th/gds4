﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[System.Serializable]
public class Interactions
{
	public int id;
	public List<string> cards;

	static bool IsOdd(int n)
	{
		return n % 2 != 0;
	}

	public bool WinsAgainst(int instigator, int target)
	{
		if (target == -1) return true;
		// Way overkill but this will solve for all
		foreach (int num in Enumerable.Range(1, cards.Count - 1).Where(IsOdd))
		{
			if ((instigator + num) % cards.Count == target) return true;
		}
		return false;
	}
};
