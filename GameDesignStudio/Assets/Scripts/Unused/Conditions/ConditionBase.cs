﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ConditionBase : ScriptableObject {

	public new string name;
	public string description;
	public bool available = true;

	public abstract bool MetCondition(GameManager gm, int instigator);
}