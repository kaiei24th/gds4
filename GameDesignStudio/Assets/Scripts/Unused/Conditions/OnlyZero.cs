﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "OnlyZero", menuName = "Conditions/OnlyZero")]
public class OnlyZero : ConditionBase
{
	public override bool MetCondition(GameManager gm, int instigator)
	{
		if(gm.players[instigator].earned != 0) return false;
		for (int i = 0; i < gm.players.Count; ++i)
		{
			if(instigator == i) continue;
			if(gm.players[i].earned == gm.players[instigator].earned)
			{
				return false;
			}
		}
		return true;
	}
}