﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewPlayerReferences : MonoBehaviour {

    public int idx;
    public Text playerIdx;
    public Text nameText;

    [HideInInspector] public LobbyManager lm;

    public void RemovePlayer()
    {
        lm.RemovePlayer(idx);
    }
}