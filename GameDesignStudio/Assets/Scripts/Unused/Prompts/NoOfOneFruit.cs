﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "NoOfOneFruit", menuName = "Prompts/NoOfOneFruit")]
public class NoOfOneFruit : PromptBase
{
	public int choosenAmount;
	public string choosenFruit;
	public List<string> fruitsTaken;

	private int CheckCount()
	{
		int count = 0;
		foreach(string fr in fruitsTaken)
		{
			if(fr == choosenFruit)
			{
				count++;
			}
		}
		return count;
	}

	public override void Create(GameManager gm)
	{
		ResetValues();
		int idx = Random.Range(0, gm.GetCurrentDeck().cards.Count);
		choosenFruit = gm.GetCurrentDeck().cards[idx];
		choosenAmount = Random.Range(2, gm.players.Count - 1);
		fruitsTaken = new List<string>();
	}

	public override void AddFruit(GameManager gm, int fruit)
	{
		string fruitName = gm.GetCardName(fruit);
		fruitsTaken.Add(fruitName);
		int count = CheckCount();
		complete = (count > choosenAmount) ? 0 : (float)count / (float)choosenAmount;
	}

	public override string GetRequirement(GameManager gm)
	{
		return "-Order-\nI was exactly " + choosenAmount.ToString() 
			+ " " + choosenFruit + "s";
	}
}