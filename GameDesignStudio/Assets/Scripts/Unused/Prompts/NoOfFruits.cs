﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "NoOfFruits", menuName = "Prompts/NoOfFruits")]
public class NoOfFruits : PromptBase
{
	public List<string> fruits;

	public override void Create(GameManager gm)
	{
		ResetValues();
		// Get a random amount of fruits as good fruits
		int amount = Random.Range(2, gm.GetCurrentDeck().cards.Count - 1);
		System.Random rnd = new System.Random();
		fruits = gm.GetCurrentDeck().cards.OrderBy(x => rnd.Next()).Take(amount).ToList();
	}

	public override void AddFruit(GameManager gm, int fruit)
	{
		if(fruits.Contains(gm.GetCardName(fruit)))
		{
			goodAttempts++;
		}
		else
		{
			badAttempts++;
		}
		UpdateCompleteness();
	}

	public override string GetRequirement(GameManager gm)
	{
		string fr = "";
		for (int i = 0; i < fruits.Count - 1; ++i)
		{
			fr += " " + fruits[i] + "s,";
		}
		fr = fr.Remove(fr.Length - 1);
		fr += " and " + fruits[fruits.Count - 1] + "s";
		return "-Order-\nI only want" + fr;
	}
}