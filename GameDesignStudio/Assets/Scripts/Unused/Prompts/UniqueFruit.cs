﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "UniqueFruit", menuName = "Prompts/UniqueFruit")]
public class UniqueFruit : PromptBase
{
	public List<string> fruitsAccepted;

	public override void Create(GameManager gm)
	{
		ResetValues();
		fruitsAccepted = new List<string>();
	}
	public override void AddFruit(GameManager gm, int fruit)
	{
		string fr = gm.GetCardName(fruit);
		if (fruitsAccepted.Contains(fr)) badAttempts+= 2;
		else goodAttempts++;
		fruitsAccepted.Add(fr);
		UpdateCompleteness();
	}

	public override string GetRequirement(GameManager gm)
	{
		return "-Order-\nI don't want duplicate fruits";
	}
}
