﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PromptBase : ScriptableObject {

	// How much is customer liking the blend?
	// Basically who is winning
	// Bad = 0
	// Good = 1
	public float complete;

	// The minimum amount required for mixers to win
	public float minimum = 0.5f;

	// Good fruits
	protected int goodAttempts;
	// Bad fruits
	protected int badAttempts;

	public new string name;

	protected void ResetValues()
	{
		complete = 0;
		goodAttempts = 0;
		badAttempts = 0;
	}

	protected void UpdateCompleteness()
	{
		complete = (float)goodAttempts / (float)(goodAttempts + badAttempts);
		//Debug.Log(complete);
	}

	// Each prompt my require special initializations
	public abstract void Create(GameManager gm);

	// This will sway the completeness of blend
	public abstract void AddFruit(GameManager gm, int fruit);

	// The requirement may be dynamic
	public abstract string GetRequirement(GameManager gm);
}
