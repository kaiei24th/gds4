﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NextTurnReferences : MonoBehaviour {

	public Text nextPlayerText;

	public GameManager gm;

	// Use this for initialization
	void Start () {
		gm = FindObjectOfType<GameManager>();
		UpdateSceneInfo();
	}
	
	void UpdateSceneInfo()
	{
		nextPlayerText.text = "Pass the phone to " + gm.GetCurrentPlayer().name;
	}

	public void NextTurn()
	{
		gm.LoadScene(SceneCtrl.Scenes.PassThePhone);
	}
}