﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DoublePoints", menuName = "Skills/DoublePoints")]
public class DoublePoints : SkillBase
{
// 	static readonly string[] bot = {
// 		"AWESOME",
// 		"IMPRESSIVE",
// 		"AMAZING",
// 		"MAGNIFICENT",
// 		"EYE-POPPIN",
// 		"PHENOMENAL",
// 		"MAJESTIC",
// 		"PRODIGIOUS",
// 		"MIRACULOUS",
// 		"SUPERHUMAN",
// 		"DIVINE",
// 		"INSANE"
// 	};

	public override void EnableSkill(GameManager gm, int instigator, int target)
	{
		AttachSkill(gm, target);
	}

	public override void ActivateSkill(GameManager gm, int instigator, int target)
	{
// 		float gain = gm.players[target].earned;
// 		gm.players[target].score += gain;
// 		gm.players[target].earned *= 2;
// 		string log;
// 		if(gain <= 1) log = "Uh oh, that wasn't the best use of that 2x";
// 		else if (gain <= gm.players.Count)
// 			log = "That 2x oof, Well... Better than nothing amirite?";
// 		else
// 		{
// 			string str = bot[Random.Range(0, bot.Length)];
// 			log = "That 2x was " + str + "!";
// 		}
// 		gm.players[target].AddLog(log);
	}

	public override bool CanBeActivated(GameManager gm, int instigator, int target)
	{
		return false;
	}

	public override bool CanBeActivatedOn(GameManager gm, int instigator, int target)
	{
		return false;
	}
}