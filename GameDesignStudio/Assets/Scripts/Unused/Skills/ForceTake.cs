﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ForceTake", menuName = "Skills/ForceTake")]
public class ForceTake : SkillBase
{
	public int fruitSelected;

	public override void EnableSkill(GameManager gm, int instigator, int target)
	{
		AttachSkill(gm, target);
		fruitSelected = Random.Range(0, gm.decks[gm.currentDeck].cards.Count);
	}

	public override void ActivateSkill(GameManager gm, int instigator, int target)
	{
// 		for(int i = 0; i < gm.ptr.fruits.Length; ++i)
// 		{
// 			if (i == fruitSelected)
// 			{
// 				string fruit = gm.decks[gm.currentDeck].cards[i];
// 				gm.GetCurrentPlayer().AddLog("You have been forced to " +
// 					"take the " + fruit + " fruit");
// 			}
// 			else
// 			{
// 				gm.ptr.fruits[i].SetActive(false);
// 			}
// 		}
	}

	public override bool CanBeActivated(GameManager gm, int instigator, int target)
	{
		return gm.currentPlayer < gm.players.Count - 1;
	}

	public override bool CanBeActivatedOn(GameManager gm, int instigator, int target)
	{
		return gm.currentPlayer < target;
	}
}