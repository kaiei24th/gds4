﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PreventWins", menuName = "Skills/PreventWins")]
public class PreventWins : SkillBase {

	public override void EnableSkill(GameManager gm, int instigator, int target)
	{
		AttachSkill(gm, target);
	}

	public override void ActivateSkill(GameManager gm, int instigator, int target)
	{
// 		float lose = gm.players[target].earned;
// 		gm.players[target].score -= lose;
// 		gm.players[target].earned = 0;
// 		gm.players[target].AddLog("You were denied " + lose + 
// 			" points last turn, Unlucky");
	}

	public override bool CanBeActivated(GameManager gm, int instigator, int target)
	{
		return true;
	}

	public override bool CanBeActivatedOn(GameManager gm, int instigator, int target)
	{
		return true;
	}
}