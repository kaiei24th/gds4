﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PickFirst", menuName = "Skills/PickFirst")]
public class PickFirst : SkillBase
{

	public override void EnableSkill(GameManager gm, int instigator, int target)
	{
		AttachSkill(gm, target);
	}

	public override void ActivateSkill(GameManager gm, int instigator, int target)
	{
// 		PlayerData pd = gm.players[target];
// 		pd.AddLog("Congrats you're at the front now, hope you bought a skill");
// 		gm.players.RemoveAt(target);
// 		gm.players.Insert(0, pd);
	}

	public override bool CanBeActivated(GameManager gm, int instigator, int target)
	{
		return true;
	}

	public override bool CanBeActivatedOn(GameManager gm, int instigator, int target)
	{
		return true;
	}
}