﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SkillBase : ScriptableObject {

	public enum TargetSelection
	{
		Self,
		Others,
		All
	};

	public enum ApplicationType
	{
		BeginRound,
		DuringTurn,
		EndRound,
		EndGame
	}

	public new string name;
	public string description;
	// Swapping the order of the players screws this up
	public int instigator;

	// What can be targeted with this skill?
	// Should we open up the target selection or restrict it?
	public TargetSelection target;

	// Hint for how good is this skill
	// Scale from 1 - 5
	public int powerLevelHint;

	// How many turns should this be used?
	public int applicationTime = 1;

	// When is this skill triggered?
	public ApplicationType appType;

	public void AttachSkill(GameManager gm, int target)
	{
		//gm.players[target].skillsApplied.Add(this);
	}

	// Putting the skill in the right place
	public abstract void EnableSkill(GameManager gm, int instigator, int target);

	// Doing the actual skill
	public abstract void ActivateSkill(GameManager gm, int instigator, int target);

	// Can we actually use the skill?
	public abstract bool CanBeActivated(GameManager gm, int instigator, int target);

	// Can the skil be used on a player?
	public abstract bool CanBeActivatedOn(GameManager gm, int instigator, int target);
}