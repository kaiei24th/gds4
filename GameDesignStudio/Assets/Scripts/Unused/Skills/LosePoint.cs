﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LosePoint", menuName = "Skills/LosePoint")]
public class LosePoint : SkillBase
{
	public override void EnableSkill(GameManager gm, int instigator, int target)
	{
		AttachSkill(gm, target);
	}

	public override void ActivateSkill(GameManager gm, int instigator, int target)
	{
// 		gm.players[target].score--;
// 		gm.players[target].earned--;
// 		gm.players[target].AddLog("You just straight up lost a point");
	}

	public override bool CanBeActivated(GameManager gm, int instigator, int target)
	{
		return true;
	}

	public override bool CanBeActivatedOn(GameManager gm, int instigator, int target)
	{
		return true;
	}
}