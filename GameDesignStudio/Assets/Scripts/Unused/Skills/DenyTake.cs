﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DenyTake", menuName = "Skills/DenyTake")]
public class DenyTake : SkillBase {

	public int fruitDenied;

	public override void EnableSkill(GameManager gm, int instigator, int target)
	{
		AttachSkill(gm, target);
		fruitDenied = Random.Range(0, gm.decks[gm.currentDeck].cards.Count);
	}

	public override void ActivateSkill(GameManager gm, int instigator, int target)
	{
// 		gm.ptr.fruits[fruitDenied].SetActive(false);
// 		string fruit = gm.decks[gm.currentDeck].cards[fruitDenied];
// 		gm.GetCurrentPlayer().AddLog("You have been denied the " + fruit + " fruit");
	}

	public override bool CanBeActivated(GameManager gm, int instigator, int target)
	{
		return gm.currentPlayer < gm.players.Count - 1;
	}

	// Can only use on player if it is after their turn
	public override bool CanBeActivatedOn(GameManager gm, int instigator, int target)
	{
		return gm.currentPlayer < target;
	}
}
