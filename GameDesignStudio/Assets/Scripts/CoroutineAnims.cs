﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoroutineAnims {

	public static List<Task> runningTasks = new List<Task>();

	public static float animationSpeed = 2f;

	public static IEnumerator PendulumMotion(float waitTimer, float animTime,
		float swingSpeed, float swingPower, AnimationCurve curve, 
		System.Action<float> callback)
	{
		yield return new WaitForSeconds(waitTimer / animationSpeed);
		float time = 0;
		float currentAmplitude = swingSpeed;
		float endCurveTime = curve.keys[curve.length - 1].time;

		while(true)
		{
			time += Time.deltaTime * animationSpeed;
			callback(currentAmplitude * Mathf.Sin(time * swingSpeed));
			currentAmplitude = swingPower * 
				(1 - curve.Evaluate(time / animTime));
			if (time >= animTime) break;
			yield return new WaitForSeconds(Time.deltaTime);
		}
	}

	public static IEnumerator PrintString(float waitTimer, float animTime, 
		string str, System.Action<string> callback)
	{
		yield return new WaitForSeconds(waitTimer / animationSpeed);
		float time = 0;
		string tempString = "";
		while(true)
		{
			time = (time > animTime) ? animTime : time + (Time.deltaTime * animationSpeed);
			float pct = (time / animTime) * str.Length;
			tempString = str.Substring(0, (int)pct);
			callback(tempString);

			if (time >= animTime) break;
			yield return new WaitForSeconds(Time.deltaTime);
		}
	}

	public static IEnumerator LerpVector2(float waitTimer, float animTime, 
		AnimationCurve curve, Vector2 startVector, Vector2 endVector, 
		System.Action<Vector2> callback)
	{
		yield return new WaitForSeconds(waitTimer / animationSpeed);
		float time = 0;
		float maxTime = curve[curve.length - 1].time;
		while (true)
		{
			time = (time > animTime) ? animTime : time + 
				(Time.deltaTime * animationSpeed);
			callback(Vector2.LerpUnclamped(startVector, endVector,
				curve.Evaluate(time / animTime)));
			if (time >= animTime) break;
			yield return new WaitForSeconds(Time.deltaTime);
		}
	}

	public static IEnumerator LerpVector3(float waitTimer, float animTime, 
		AnimationCurve curve, Vector3 startVector, Vector3 endVector, 
		System.Action<Vector3> callback)
	{
		yield return new WaitForSeconds(waitTimer / animationSpeed);
		float time = 0;
		float maxTime = curve[curve.length - 1].time;
		while (true)
		{
			time = (time > animTime) ? animTime : time + 
				(Time.deltaTime * animationSpeed);
			callback(Vector3.LerpUnclamped(startVector, endVector,
				curve.Evaluate(time / animTime)));
			if (time >= animTime) break;
			yield return new WaitForSeconds(Time.deltaTime);
		}
	}

	public static IEnumerator LerpFloat(float waitTimer, float animTime, 
		AnimationCurve curve, float startFloat, float endFloat, 
		System.Action<float> callback)
	{
		yield return new WaitForSeconds(waitTimer / animationSpeed);
		float time = 0;
		float maxTime = curve[curve.length - 1].time;
		while (true)
		{
			time = (time > animTime) ? animTime : time + 
				(Time.deltaTime * animationSpeed);
			callback(Mathf.LerpUnclamped(startFloat, endFloat,
				curve.Evaluate(time / animTime)));
			if (time >= animTime) break;
			yield return new WaitForSeconds(Time.deltaTime);
		}
	}

	public static IEnumerator Wait(float waitTimer)
	{
		yield return new WaitForSeconds(waitTimer / animationSpeed);
	}

	public static Task CoroutineHandler(IEnumerator coroutine)
	{
		return new Task(coroutine);
	}

	public static Task CoroutineHandler(IEnumerator coroutine,
		Task.FinishedHandler handlers = null)
	{
		Task t = new Task(coroutine);
		t.Finished += handlers;
		return t;
	}

	public static Task CoroutineHandler(IEnumerator coroutine,
		List<Task.FinishedHandler> handlers = null)
	{
		Task t = new Task(coroutine);
		foreach (Task.FinishedHandler handle in handlers)
		{
			t.Finished += handle;
		}
		return t;
	}
}