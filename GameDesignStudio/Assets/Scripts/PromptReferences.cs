﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PromptReferences : MonoBehaviour {

	public GameManager gm;

	public List<Text> promptText;

	void Start () {
		gm = FindObjectOfType<GameManager>();
		UpdatePrompts();
	}
	
	void UpdatePrompts()
	{
		List<int> choosenPrompts = new List<int>();
		foreach(Text p in promptText)
		{
			int chosen;
			while(true)
			{
				chosen = Random.Range(0, gm.availableHints.Count);
				if(!choosenPrompts.Contains(chosen)) break;
				if(gm.availableHints.Count < 
					promptText.Count + choosenPrompts.Count)
				{
					Debug.Log("Something is duped");
					break;
				}
			}
			string choosenPrompt = gm.availableHints[chosen];
			p.text = choosenPrompt;
			choosenPrompts.Add(chosen);
		}
	}

	public void AddPrompt(Text text)
	{
		gm.choosenHints.Add(text.text);
		gm.availableHints.Remove(text.text);

		// Set the prompt of the player
		// Yeah we are duplicating it but this avoids idx mis-matches
		gm.GetCurrentPlayer().choosenPrompt = text.text;

		GoNext();
	}

	void GoNext()
	{
		gm.SetNextPlayer(false);
		gm.LoadScene(SceneCtrl.Scenes.PassThePhone);
	}
}
